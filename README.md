# 技术选型

## 1、后端
	核心框架：Spring Boot 2.0
	安全框架：Spring Security
	模板引擎：Thymeleaf
	持久层框架：MyBatisPlus
	数据库连接池：HikariCP
	缓存框架：redis
	日志管理：Log4j2  
	容器：undertow
	工具类：lombok、commons、guava
	文档：Swagger2
	
	JDK:1.8
	构建：Maven
	数据库：MAriaDb
	容器：docker

## 2、前端
	暂时考虑使用hplus
	
## 3、其他
    寻找志同道合自人共同完善
    qq：45754605