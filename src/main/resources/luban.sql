/*
Navicat MariaDB Data Transfer

Source Server         : 鲁班数据库
Source Server Version : 100307
Source Host           : 123.207.8.14:3306
Source Database       : luban

Target Server Type    : MariaDB
Target Server Version : 100307
File Encoding         : 65001

Date: 2018-06-20 18:28:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for sys_attach
-- ----------------------------
DROP TABLE IF EXISTS `sys_attach`;
CREATE TABLE `sys_attach` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `user_id` int(10) unsigned DEFAULT NULL COMMENT '用户id',
  `source_name` varchar(128) NOT NULL COMMENT '源文件名',
  `file_name` varchar(128) NOT NULL COMMENT '文件名',
  `file_type` varchar(64) DEFAULT NULL COMMENT '文件类型',
  `file_path` varchar(128) NOT NULL COMMENT '磁盘路径',
  `file_size` bigint(20) unsigned NOT NULL COMMENT '文件大小',
  `attach_url` varchar(128) NOT NULL COMMENT '附件地址',
  `relation_key` varchar(64) DEFAULT NULL COMMENT '关联模块',
  `create_time` timestamp NULL DEFAULT current_timestamp() COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统-附件表';

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '父id',
  `name` varchar(64) NOT NULL COMMENT '名称',
  `value` varchar(128) DEFAULT NULL COMMENT '值',
  `type` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '类型{1:系统,2:业务,3:其他}',
  `status` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '状态{0:禁用,1:启用}',
  `sort_no` int(10) unsigned DEFAULT 0 COMMENT '排序',
  `remark` varchar(128) DEFAULT '' COMMENT '备注',
  `create_time` timestamp NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  `version` int(10) unsigned DEFAULT 0 COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统-字典表';

-- ----------------------------
-- Table structure for sys_olog
-- ----------------------------
DROP TABLE IF EXISTS `sys_olog`;
CREATE TABLE `sys_olog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(64) NOT NULL COMMENT '操作名称',
  `request_uri` varchar(128) NOT NULL COMMENT '请求URI',
  `request_method` varchar(64) NOT NULL COMMENT '请求方式',
  `request_parameters` varchar(255) DEFAULT NULL COMMENT '请求参数',
  `execute_milliseconds` bigint(20) unsigned NOT NULL COMMENT '执行时间',
  `operate_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp() COMMENT '操作时间',
  `operate_user` varchar(64) DEFAULT NULL COMMENT '操作人',
  `operate_user_id` int(10) unsigned DEFAULT NULL COMMENT '操作人id',
  `operate_result` tinyint(1) unsigned DEFAULT 1 COMMENT '操作结果{0:失败,1:成功}',
  `operate_message` varchar(255) DEFAULT NULL COMMENT '操作结果消息',
  `class_name` varchar(64) DEFAULT NULL COMMENT '执行类名称',
  `class_methed` varchar(64) DEFAULT NULL COMMENT '执行类方法',
  `client_info` varchar(255) DEFAULT NULL COMMENT '客户端信息',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统-操作日志表';

-- ----------------------------
-- Table structure for sys_org
-- ----------------------------
DROP TABLE IF EXISTS `sys_org`;
CREATE TABLE `sys_org` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '父id',
  `code` varchar(32) NOT NULL COMMENT '简称',
  `name` varchar(64) DEFAULT NULL COMMENT '全称',
  `sort_no` int(10) unsigned DEFAULT 0 COMMENT '排序',
  `remark` varchar(128) DEFAULT '' COMMENT '备注',
  `create_time` timestamp NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  `version` int(10) unsigned DEFAULT 0 COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统-组织表';

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` int(10) unsigned NOT NULL DEFAULT 0 COMMENT '父id',
  `name` varchar(64) NOT NULL COMMENT '资源名称',
  `url` varchar(128) DEFAULT NULL COMMENT '资源地址',
  `type` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '资源类型{1:菜单,2:按钮,3:其他}',
  `status` tinyint(1) unsigned NOT NULL DEFAULT 1 COMMENT '资源状态{0:禁用,1:启用}',
  `method` tinyint(1) unsigned DEFAULT NULL COMMENT '资源请求类型{0:all,1:get,2:post,3:put,4:delete,5:other}',
  `is_show` tinyint(1) unsigned DEFAULT 0 COMMENT '是否展示{0:隐藏,1:展示}',
  `permission` varchar(64) DEFAULT NULL COMMENT '权限标志',
  `icon` varchar(64) DEFAULT NULL COMMENT '图标',
  `sort_no` int(10) unsigned DEFAULT 0 COMMENT '排序',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注',
  `create_time` timestamp NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  `version` int(10) unsigned DEFAULT 0 COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统-资源表';

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `org_id` int(10) unsigned NOT NULL COMMENT '组织id',
  `name` varchar(64) NOT NULL COMMENT '角色名称',
  `status` tinyint(1) unsigned DEFAULT 0 COMMENT '角色状态{0:禁用,1:正常,2:锁定}',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注',
  `create_time` timestamp NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  `version` int(10) unsigned DEFAULT 0 COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统-角色表';

-- ----------------------------
-- Table structure for sys_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_resource`;
CREATE TABLE `sys_role_resource` (
  `role_id` int(10) unsigned NOT NULL COMMENT '角色id',
  `resource_id` int(10) unsigned NOT NULL COMMENT '资源id',
  PRIMARY KEY (`role_id`,`resource_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统-角色资源映射表';

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `org_id` int(10) unsigned NOT NULL COMMENT '组织id',
  `role_id` int(10) unsigned NOT NULL COMMENT '角色id',
  `username` varchar(64) NOT NULL COMMENT '用户名',
  `password` varchar(128) NOT NULL COMMENT '密码',
  `name` varchar(64) DEFAULT NULL COMMENT '姓名',
  `sex` tinyint(1) unsigned DEFAULT 0 COMMENT '性别{0:未知,1:男,2:女}',
  `mobile` varchar(20) DEFAULT NULL COMMENT '电话',
  `email` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `avator` varchar(128) DEFAULT NULL COMMENT '头像',
  `status` tinyint(1) unsigned DEFAULT 0 COMMENT '用户状态{0:禁用,1:正常,2:锁定}',
  `remark` varchar(128) DEFAULT NULL COMMENT '备注',
  `create_time` timestamp NULL DEFAULT current_timestamp() COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp() COMMENT '更新时间',
  `version` int(10) unsigned DEFAULT 0 COMMENT '乐观锁',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统-用户表';
