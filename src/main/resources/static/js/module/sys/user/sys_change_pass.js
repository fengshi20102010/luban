require(['jquery', 'layer', 'jquery.validate.methods', 'jquery.icheck', 'main'], function($, layer) {
    //初始化layer
    layer.config({
        path: '/components/layer/3.0.1/'
    });
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%'
    });
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#form1").validate({
        rules: {
        		oldpassword : {
                    required: true,
                	minlength: 6,
                	maxlength: 18
                },
                newpassword : {
                	required: true,
                	minlength: 6,
                	maxlength: 18
                },
                newpassword2 : {
                	required: true,
                	minlength: 6,
                	maxlength: 18,
                	equalTo: '#newpassword'
                }
        },
        messages: {
        	oldpassword : {
                required: icon + '原始密码必填',
                minlength: icon + '最小{}位',
                maxlength: icon + '最大{}位',
            },
            newpassword : {
            	required: icon + '新密码必填',
            	minlength: icon + '最小{}位',
            	maxlength: icon + '最大{}位'
            },
            newpassword2 : {
            	required: icon + '新密码必填',
            	minlength: icon + '最小{}位',
            	maxlength: icon + '最大{}位',
            	equalTo: icon + '和新密码不一致',
            }
        },
        highlight: function(element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error")
        },
        success: function(element) {
            element.closest(".form-group").removeClass("has-error").addClass("has-success")
        },
        errorElement: "span",
        errorPlacement: function(element, r) {
            element.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
        },
        errorClass: "help-block m-b-none",
        validClass: "help-block m-b-none",
        onkeyup:false,
        submitHandler:function(form){
            $.ajax({
                url:window.location.href,
                dataType:'json',
                type:'post',
                data: $('#form1').serialize(),
                success:function(data){
                    if(data.success){
                    	setTimeout(layer.msg('修改成功！', {icon: 1, time: 1000}), 2000);
                    	layerClose();
                        return;
                    }else{
                        layer.msg(data.message,{icon:2,time:1000});
                    }
                }
            })
        }
    });
});