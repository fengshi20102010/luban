require(['bootstrap-table.zh-CN', 'layer', 'moment', 'main'], function($, layer, moment) {
    layer.config({
        path: '/components/layer/3.0.1/'
    });
    
    $('#bootstrap-table').bootstrapTable({
        contentType: "application/x-www-form-urlencoded",
        method: 'post',
        toolbar: '#toolbar',
        iconSize: "outline",
        icons: {
            refresh: "glyphicon-repeat",
            toggle: "glyphicon-list-alt",
            columns: "glyphicon-list"
        },
        striped: true,
        cache: false,
        pagination: true,
        sortable: false,
        pageNumber: 1,
        pageSize: 5,
        pageList: [10, 25, 50, 100],
        url: window.location.href,
        queryParamsType: '',
        queryParams: function(params){
            $('#pageNumber').val(params.pageNumber);
            $('#pageSize').val(params.pageSize);
            $('#sortName').val(params.sortName);
            $('#sortOrder').val(params.sortOrder);
            return $('#searchForm').serialize();
        },
        sidePagination: "server", 
        strictSearch: true,
        showColumns: true, 
        showRefresh: true, 
        minimumCountColumns: 2, 
        searchOnEnterKey: true,
        pagination: true,
        sortable: true, 
        sortName: "id",
        sortOrder: "desc",
        columns: [
            {
                checkbox: true,
                align: 'center'
            }, 
            {
                field: 'id',
                align: 'center',
                sortable : true
            }, 
            {
            	field: 'avator',
            	align: 'center'
            }, 
            {
            	field: 'orgName',
            	align: 'center'
            }, 
            {
            	field: 'roleName',
            	align: 'center'
            }, 
            {
            	field: 'username',
            	align: 'center',
            	sortable : true
            }, 
            {
            	field: 'name',
            	align: 'center',
            	sortable : true
            }, 
            {
                field: 'sex',
                align: 'center',
                formatter: function(value, row, index) {
                	var content = '<span class="label label-info">未知</span>';
                	if(value == 1){
                        content = '<span class="label label-info">男</span>';
                	} else if(value == 2){
                        content = '<span class="label label-info">女</span>';
                	}
                	return content;
                }
            }, 
            {
            	field: 'mobile',
            	align: 'center'
            }, 
            {
            	field: 'email',
            	align: 'center'
            }, 
            {
                field: 'status',
                align: 'center',
                formatter: function(value, row, index) {
                	var content = '<span class="label label-info">未知</span>';
                	if(value == 0){
                        content = '<span class="label label-info">禁用</span>';
                	}else if(value == 1){
                        content = '<span class="label label-info">启用</span>';
                	} else if(value == 2){
                		content = '<span class="label label-info">锁定</span>';
                	}
                	return content;
                }
            }, 
            {
                field: 'createTime',
                align: 'center',
                formatter: function(value, row, index) {
                	return moment(value).format('YYYY-MM-DD');
                }
            }, 
            {
                field: 'id',
                align: 'center',
                formatter: function(value, row, index){
                    var content = '';
                    content += '<a title="详情" href="javascript:;" class="ml-5 view" style="text-decoration:none"><i class="fa fa-eye"></i></a>';
                    content += '<a title="编辑" href="javascript:;" class="ml-5 edit" style="text-decoration:none" ><i class="fa fa-edit"></i></a>';
                   	content += '<a title="删除" href="javascript:;" class="ml-5 del" style="text-decoration:none"><i class="fa fa-edit"></i></a>'
                   	content += '<a title="重置密码" href="javascript:;" class="ml-5 pass" style="text-decoration:none"><i class="fa fa-key"></i></a>';
                    if('' == content) content = '无权限';
                    return content;
                }
            }
        ]
    });

    // 搜索
    $('#search').on('click', function() {
        $('#bootstrap-table').bootstrapTable('refresh');
    });
    
    // 新增
    $('#add').on('click', function() {
        var title = '新增系统用户';
        var url = 'add';
        layerShow(title, url, "800", "500");
    });

    // 编辑
    $('#bootstrap-table').delegate('.edit', 'click', function() {
        var index = $(this).parents('tr').data('index');
        var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
        var title = '编辑系统用户';
        var url = 'edit?id=' + id;
        layerShow(title, url, 800, 500);
    });

    // 查看
    $('#bootstrap-table').delegate('.view', 'click', function() {
        var index = $(this).parents('tr').data('index');
        var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
        var title = '查看系统用户';
        var url = 'view?id=' + id;
        layerShow(title, url);
    });
    
    // 重置密码
	$('#bootstrap-table').delegate('.pass', 'click', function() {
		var index = $(this).parents('tr').data('index');
        var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
		layer.confirm('确认要为该用户重置密码吗？',function(index){
			$.post('resetPass', {"id": id}, function(d){
				if(d.success){
					layer.msg('操作成功！', {icon: 1, time: 1000});
                    return;
				}
				layer.msg(d.message, {icon: 5, time: 1000});
			},"json");
		});
	});

	// 删除
	$('#bootstrap-table').delegate('.del', 'click', function() {
		var index = $(this).parents('tr').data('index');
        var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
		layer.confirm('确认要删除该用户吗？',function(index){
			$.post('del', {"id": id}, function(d){
				if(d.success){
					layer.msg('操作成功！', {icon: 1, time: 1000});
					setTimeout($('#bootstrap-table').bootstrapTable('refresh'), 1000);
                    return;
				}
				layer.msg(d.message, {icon: 5, time: 1000});
			},"json");
		});
	});
});