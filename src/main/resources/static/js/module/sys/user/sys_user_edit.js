require(['jquery', 'layer', 'jquery.validate.methods', 'jquery.icheck', 'cropper', 'main'], function($, layer) {
    //初始化layer
    layer.config({
        path: '/components/layer/3.0.1/'
    });
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%'
    });
    
    //头像
    var $avator = $('#avator');
    $avator.cropper({
      aspectRatio: 16 / 9,
      crop: function(event) {
        console.log(event.detail.x);
        console.log(event.detail.y);
        console.log(event.detail.width);
        console.log(event.detail.height);
        console.log(event.detail.rotate);
        console.log(event.detail.scaleX);
        console.log(event.detail.scaleY);
      }
    });
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#form1").validate({
        rules: {
                username : {
                    required: true,
                    remote: {
                    	url: 'checkUsername',
                    	type: 'get',
                    	dataType: "json"
                    }
                },
                name : {
                	required: true
                },
                mobile : {
                	required: true,
                	isMobile: true
                },
                email : {
                    required: true,
                    email: true
                },
                sex : {
                	required: true
                },
                status : {
                	required: true
                }
        },
        messages: {
            username : {
                required: icon + '必填',
                remote: icon + '用户名已存在'
            },
            name : {
            	required: icon + '必填'
            },
            mobile : {
            	required: icon + '必填',
            	isMobile: icon + '请填写正确的手机号码'
            },
            email : {
                required: icon + '必填',
                email: icon + '请填写正确的邮箱'
            },
            sex : {
            	required: icon + '必填'
            },
            status : {
            	required: icon + '必填'
            }
        },
        highlight: function(element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error")
        },
        success: function(element) {
            element.closest(".form-group").removeClass("has-error").addClass("has-success")
        },
        errorElement: "span",
        errorPlacement: function(element, r) {
            element.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
        },
        errorClass: "help-block m-b-none",
        validClass: "help-block m-b-none",
        onkeyup:false,
        submitHandler:function(form){
            $.ajax({
                url:window.location.href,
                dataType:'json',
                type:'post',
                data: $('#form1').serialize(),
                success:function(data){
                    if(data.success){
                        parent.$('#search').click();
                        layerClose();
                        return;
                    }else{
                        layer.msg(data.message,{icon:2,time:1000});
                    }
                }
            })
        }
    });
});