require(['jquery', 'main'], function($) {
	
	// 修改密码
	$('#changePass').on('click', function(){
		var title = '修改密码';
		var url = '/sys/sysUser/changePass';
		layerShow(title,url,"800","300");
	});
	
	// 我的详情
	$('#personal').on('click', function(){
		var title = '我的详情';
		var url = '/sys/sysUser/personal';
		layerShow(title,url,"800","500");
	});
	
	// 退出登陆
	$('.logout').on('click', function(){
		layer.confirm('确认退出登陆吗？', {icon: 3, title:'退出登陆'}, function(index){
			layer.close(index);
			window.location.href = '/logout';
		});
	});
});