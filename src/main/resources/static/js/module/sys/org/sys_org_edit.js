require(['jquery', 'layer', 'jquery.validate.methods', 'jquery.icheck', 'main'], function($, layer) {
    //初始化layer
    layer.config({
        path: '/components/layer/3.0.1/'
    });
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%'
    });
    var icon = "<i class='fa fa-times-circle'></i> ";
    $("#form1").validate({
        rules: {
            code : {
                required: true
            },
            name : {
                required: true,
            },
            remark : {
                required: true,
            }
        },
        messages: {
            code : {
                required: icon + '必填',
            },
            name : {
                required: icon + '必填'
            },
            remark : {
                required: icon + '必填'
            }
        },
        highlight: function(element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error")
        },
        success: function(element) {
            console.log(element)
            element.closest(".form-group").removeClass("has-error").addClass("has-success")
        },
        errorElement: "span",
        errorPlacement: function(element, r) {
            element.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
        },
        errorClass: "help-block m-b-none",
        validClass: "help-block m-b-none",
        onkeyup:false,
        submitHandler:function(form){
            $.ajax({
                url:window.location.href,
                dataType:'json',
                type:'post',
                data: $('#form1').serialize(),
                success:function(data){
                    if(data.success){
                        parent.$('#search').click();
                        layerClose();
                        return;
                    }else{
                        layer.msg(data.message,{icon:2,time:1000});
                    }
                }
            })
        }
    });

    //单选框改变触发
    $('input:radio[name="radio"]').click(function(){
        var val = $(this).val();
        if(val && val == 1){
            $("#pid").removeAttr("disabled");

        } else {
            $("#pid").attr("disabled",true);
        }
        $("input[name='isRadio']").val(val);
    });

    $("#pid").change(function () {
        var val = $(this).val();
        if(!val){
            val = 0;
        }
        $("input[name='pid']").val(val);
    });
});