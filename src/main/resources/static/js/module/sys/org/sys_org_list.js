require(['jquery', 'layer', 'jsTree', 'main'], function($, layer) {
    layer.config({
        path: '/components/layer/3.0.1/'
    });
    loadOrgTree();

    // 新增
    $('#nestable-menu').delegate('.add', 'click', function() {
        var title = '添加部门信息';
        var url = 'add';
        layerShow(title, url, 800, 500);
    });

    //删除
    $('#nestable-menu').delegate('.del', 'click', function() {
        var ids = [];
        $(".jstree-node").each(function(){
            if( $(this).attr("aria-selected") == 'true' ){
                ids.push({id:$(this).attr("id"),name:$(this).find(".edit").html()})
            }
        });
        if(ids.length != 1){
            layer.msg("请选择需要删除的部门",{icon:2,time:2000});
            return;
        }

        var org = ids[0];
        layer.confirm('确认要删除'+org.name+'吗？',function(){
            $.post('del', {"id": org.id}, function(d){
                if(d.success){
                    layer.msg('操作成功！', {icon: 1, time: 2000});
                    loadOrgTree();
                    return;
                }
                layer.msg(d.message, {icon: 5, time: 2000});
            },"json");
        });


    });

    //编辑
    $("#orgList").delegate("a.edit","dblclick",function(){
        var id = $(this).parents("li").attr("id");
        var title = '编辑部门信息';
        var url = 'edit?id='+id;
        layerShow(title, url, 800, 500);
    });

});

//加载树
function loadOrgTree() {


    $.ajax({
        type: "POST",
        url: "/sys/sysOrg/getTreeList",
        data: {},
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (d) {
            var data = [];
            for(var i=0;i<d.length;i++) {
                var org = {};
                org["id"] = d[i].id;
                org["text"] = "<a href='javascript:void(0)' class='edit'>"+d[i].name+"<a>";
                if (d[i].childrenList && d[i].childrenList.length > 0) {
                    var children = [];
                    for (var j = 0; j < d[i].childrenList.length; j++) {
                        var child = d[i].childrenList[j];
                        children.push({id: child.id, text: "<a href='javascript:void(0)' class='edit'>"+child.name+"<a>"});
                    }
                    org["children"] = children;
                }
                data.push(org);
            }

            $('#orgList').jstree({
                'core': {
                    'data': data,
                    "themes": {
                        "dots": true,               // no connecting dots between dots
                        "responsive": false        //无响应
                    },
                    'multiple': false,              //设置其为没有多选
                    'check_callback': true,          //设置其true.可以进行文本的修改。
                },
                'types': {                         //这里就是图片的显示格式
                    "default": {
                        "icon": "fa fa-folder tree-item-icon-color icon-lg"
                    },
                    "file": {
                        "icon": "fa fa-file tree-item-icon-color icon-lg"
                    }
                },
                'plugins': [                       //插件，下面是插件的功能
                    'types',                      //可以设置其图标，在上面的一样。
                    // 'contextmenu',                //文本菜单
                    'wholerow',                   //
                    // 'sort',                       //分类
                    'unique'                      //独特----防止重复。(新添加的)
                ]
            });
        }
    });
}


