require(['jquery', 'layer', 'jquery.validate.methods', 'jquery.icheck', 'main'], function($, layer) {
    //初始化layer
    layer.config({
        path: '/components/layer/3.0.1/'
    });
    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%'
    });
    
    var icons ='\
    <ul id="icon_templ" class="icon-cc the-icons">\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-500px"></i> 500px</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-amazon"></i> amazon</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-balance-scale"></i> balance-scale</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-battery-0"></i> battery-0 <span class="text-muted">(alias)</span></li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-battery-1"></i> battery-1 <span class="text-muted">(alias)</span></li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-battery-2"></i> battery-2 <span class="text-muted">(alias)</span></li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-battery-3"></i> battery-3 <span class="text-muted">(alias)</span></li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-battery-4"></i> battery-4 <span class="text-muted">(alias)</span></li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-battery-empty"></i> battery-empty</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-battery-full"></i> battery-full</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-battery-half"></i> battery-half</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-battery-quarter"></i> battery-quarter</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-battery-three-quarters"></i> battery-three-quarters</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-black-tie"></i> black-tie</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-calendar-check-o"></i> calendar-check-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-calendar-minus-o"></i> calendar-minus-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-calendar-plus-o"></i> calendar-plus-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-calendar-times-o"></i> calendar-times-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-cc-diners-club"></i> cc-diners-club</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-cc-jcb"></i> cc-jcb</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-chrome"></i> chrome</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-clone"></i> clone</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-commenting"></i> commenting</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-commenting-o"></i> commenting-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-contao"></i> contao</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-creative-commons"></i> creative-commons</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-expeditedssl"></i> expeditedssl</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-firefox"></i> firefox</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-fonticons"></i> fonticons</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-genderless"></i> genderless</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-get-pocket"></i> get-pocket</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-gg"></i> gg</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-gg-circle"></i> gg-circle</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hand-grab-o"></i> hand-grab-o <span class="text-muted">(alias)</span></li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hand-lizard-o"></i> hand-lizard-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hand-paper-o"></i> hand-paper-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hand-peace-o"></i> hand-peace-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hand-pointer-o"></i> hand-pointer-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hand-rock-o"></i> hand-rock-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hand-scissors-o"></i> hand-scissors-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hand-spock-o"></i> hand-spock-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hand-stop-o"></i> hand-stop-o <span class="text-muted">(alias)</span></li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hourglass"></i> hourglass</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hourglass-1"></i> hourglass-1 <span class="text-muted">(alias)</span></li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hourglass-2"></i> hourglass-2 <span class="text-muted">(alias)</span></li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hourglass-3"></i> hourglass-3 <span class="text-muted">(alias)</span></li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hourglass-end"></i> hourglass-end</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hourglass-half"></i> hourglass-half</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hourglass-o"></i> hourglass-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-hourglass-start"></i> hourglass-start</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-houzz"></i> houzz</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-i-cursor"></i> i-cursor</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-industry"></i> industry</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-internet-explorer"></i> internet-explorer</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-map"></i> map</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-map-o"></i> map-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-map-pin"></i> map-pin</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-map-signs"></i> map-signs</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-mouse-pointer"></i> mouse-pointer</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-object-group"></i> object-group</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-object-ungroup"></i> object-ungroup</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-odnoklassniki"></i> odnoklassniki</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-odnoklassniki-square"></i> odnoklassniki-square</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-opencart"></i> opencart</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-opera"></i> opera</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-optin-monster"></i> optin-monster</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-registered"></i> registered</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-safari"></i> safari</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-sticky-note"></i> sticky-note</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-sticky-note-o"></i> sticky-note-o</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-television"></i> television</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-trademark"></i> trademark</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-tripadvisor"></i> tripadvisor</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-tv"></i> tv <span class="text-muted">(alias)</span></li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-vimeo"></i> vimeo</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-wikipedia-w"></i> wikipedia-w</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-y-combinator"></i> y-combinator</li>\
       <li class="fa-hover col-md-3 col-sm-4"><i class="fa fa-yc"></i> yc <span class="text-muted">(alias)</span></li>\
    </ul>\
    ';
    
    //选择菜单图标
    $('#icon').click(function(){
        var $this = $(this);
        var index = layer.open({
              type: 1,
              closeBtn:1,
              shade: [0.4,'#000'],
              shadeClose:true,
              btn:['确认','取消'],
              title: false, 
              area:'695px',
              content: icons,
              yes: function(index){
                  $this.val($(icon_templ).find('.chose').find('i').attr('class'));
                  layer.close(index);
              },
              btn2:function(index){
                  layer.close(index);
              },
              cancel: function(index){
                  layer.close(index); 
              }
          });
          
          $(icon_templ).find('li').click(function(){
          	$(this).addClass('chose').siblings().removeClass('chose');
          })
    })
    
    var errIcon = "<i class='fa fa-times-circle'></i> ";
    $("#form1").validate({
        rules: {
                name : {
                    required: true
                },
                url : {
                	required: true
                },
                type : {
                	required: true
                },
                status : {
                	required: true
                },
                method : {
                	required: true
                },
                isShow : {
                	required: true
                },
                permission : {
                	required: true
                }
        },
        messages: {
            name : {
                required: errIcon + '必填'
            },
            url : {
            	required: errIcon + '必填'
            },
            type : {
            	required: errIcon + '必填'
            },
            status : {
            	required: errIcon + '必填'
            },
            method : {
            	required: errIcon + '必填'
            },
            isShow : {
            	required: errIcon + '必填'
            },
            permission : {
            	required: errIcon + '必填'
            }
        },
        highlight: function(element) {
            $(element).closest(".form-group").removeClass("has-success").addClass("has-error")
        },
        success: function(element) {
            element.closest(".form-group").removeClass("has-error").addClass("has-success")
        },
        errorElement: "span",
        errorPlacement: function(element, r) {
            element.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
        },
        errorClass: "help-block m-b-none",
        validClass: "help-block m-b-none",
        onkeyup:false,
        submitHandler:function(form){
            $.ajax({
                url:window.location.href,
                dataType:'json',
                type:'post',
                data: $('#form1').serialize(),
                success:function(data){
                    if(data.success){
                        var index = parent.layer.getFrameIndex(window.name);
                        layer.msg('操作成功！', {icon: 1, time: 1000});
                        setTimeout(window.parent.location.reload(), 2000);
                        return;
                    }else{
                        layer.msg(data.message,{icon:2,time:1000});
                    }
                }
            })
        }
    });
});