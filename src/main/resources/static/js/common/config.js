/**
 * require 配置
 */
requirejs.config({
	urlArgs: 'bust=' + (new Date()).getTime(),
	baseUrl: '/js',
	paths: {
		'main': 'common/main',
		'contabs': 'common/contabs',
		'content': 'common/content',
		'css': 'common/css.min', //动态加载css样式
		'jquery': '../components/jquery/1.12.4/jquery.min',
		'jquery.icheck': '../components/icheck/1.0.2/icheck.min',
		'jquery.nestable': '../components/jquery.nestable/jquery.nestable',
		'jquery.metisMenu': '../components/metisMenu/2.5.2/metisMenu',
		'jquery.slimscroll': '../components/slimscroll/1.3.8/jquery.slimscroll.min',
		'jquery.validate' : '../components/jquery.validation/1.15.0/jquery.validate',
		'jquery.validate.zh-CN' : '../components/jquery.validation/1.15.0/localization/messages_zh',
		'jquery.validate.methods' : '../components/jquery.validation/1.15.0/validate-methods',
		'bootstrap': '../components/bootstrap/3.3.7/js/bootstrap.min',
		'bootstrap-table': '../components/bootstrap-table/1.11.0/bootstrap-table',
		'bootstrap-table.zh-CN': '../components/bootstrap-table/1.11.0/locale/bootstrap-table-zh-CN',
		'datetimepicker': '../components/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min',
		'datetimepicker.zh-CN': '../components/bootstrap-datetimepicker/4.17.43/js/locales/bootstrap-datetimepicker.zh-CN',
		'daterangepicker': '../components/bootstrap-daterangepicker/2.1.25/daterangepicker',
		'summernote': '../components/summernote/0.8.2/summernote',
		'summernote.zh-CN': '../components/summernote/0.8.2/lang/summernote-zh-CN',
		'lodash': '../components/lodash/4.16.4/lodash.min', //js函数库，提供很多实用函数
		'jsTree': '../components/jsTree/3.3.3/jstree',
		'webuploader': '../components/webuploader/0.1.6/webuploader',
		'umeditor': '../components/umeditor/1.2.3/umeditor',
		'umeditor.zh-CN': '../components/umeditor/1.2.3/lang/zh-cn/zh-cn',
		'ueditor': '../components/ueditor/1.4.3.3/ueditor.all',
		'ueditor.zh-CN': '../components/ueditor/1.4.3.3/lang/zh-cn/zh-cn',
		'cropper': '../components/cropper/4.0.0/cropper.min',
		'layer': '../components/layer/3.0.1/layer',
		'moment': '../components/moment/2.15.1/moment'
	},
	shim: {
		'main': {
			exports: "$",
			deps: ['bootstrap', 'contabs', 'content']
		},
		'jquery.icheck': {
			exports: "$",
			deps: ['jquery', 'css!../components/icheck/1.0.2/skins/all.css']
		},
		'jquery.nestable': {
			exports: "$",
			deps: ['jquery']
		},
		'jquery.metisMenu': {
			exports: "$",
			deps: ['jquery']
		},
		'jquery.slimscroll': {
			exports: "$",
			deps: ['jquery']
		},
		'jquery.validate': {
			exports: "$",
			deps: ['jquery']
		},
		'bootstrap': {
			exports: "$",
			deps: ['jquery']
		},
		'bootstrap-table': {
			exports: "$",
			deps: ['bootstrap', 'css!../components/bootstrap-table/1.11.0/bootstrap-table.css']
		},
		'bootstrap-table.zh-CN': {
			exports: "$",
			deps: ['bootstrap-table']
		},
		'datetimepicker': {
			exports: '$',
			deps: ['bootstrap', 'css!../components/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css']
		},
		'datetimepicker.zh-CN': {
			exports: '$',
			deps: ['datetimepicker']
		},
		'daterangepicker': {
			exports: '$',
			deps: ['bootstrap', 'css!../components/bootstrap-daterangepicker/2.1.25/daterangepicker.css']
		},
		'summernote': {
			exports: "$",
			deps: ['bootstrap', 'css!../components/summernote/0.8.2/summernote.css', 'css!../components/summernote/0.8.2/summernote-bs3.css']
		},
		'summernote.zh-CN': {
			exports: "$",
			deps: ['summernote']
		},
		'jsTree': {
			exports: "$",
			deps: ['jquery', 'css!../components/jsTree/3.3.3/themes/default/style.css']
		},
		'webuploader': {
			exports: "$",
			deps: ['jquery', 'css!../components/webuploader/0.1.6/webuploader.css']
		},
		'umeditor': {
			exports: "UM",
			deps: ['jquery', '../components/umeditor/1.2.3/third-party/template.min', '../components/umeditor/1.2.3/umeditor.config', 'css!../components/umeditor/1.2.3/themes/default/css/umeditor.css']
		},
		'umeditor.zh-CN': {
			exports: "UM",
			deps: ['umeditor']
		},
		'ueditor': {
			exports: "UE",
			deps: ['../components/ueditor/1.4.3.3/ueditor.config', 'css!../components/ueditor/1.4.3.3/themes/default/css/ueditor.css']
		},
		'ueditor.zh-CN': {
			exports: "UE",
			deps: ['ueditor']
		},
		'cropper': {
			exports: "$",
			deps: ['cropper', 'css!../components/cropper/4.0.0/cropper.min.css']
		},
		'layer': {
			exports: "layer",
			deps: ['jquery']
		}
	}
});
