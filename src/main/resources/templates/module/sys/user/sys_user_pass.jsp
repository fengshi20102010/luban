<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="renderer" content="webkit">
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<title>魔镜助手 - 重置密码</title>
		<!--[if lt IE 9]>
	    	<meta http-equiv="refresh" content="0;ie.html" />
	    <![endif]-->
		<link rel="shortcut icon" href="favicon.ico">
		<link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
		<link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
	    <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
	    <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
	</head>
	<body>
		<div class="ibox float-e-margins">
			<div class="ibox-content">
                <form class="form-horizontal m-t" id=from1>
				 	<input type="hidden" name="id" value="${item.id}">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">登录用户名：</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="username" name="username" placeholder="请输入登录用户名" readonly value="${item.username}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">新登录密码：</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" autocomplete="off" placeholder="新登录密码" name="newpassword" id="newpassword">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">再次输入新登录密码：</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" autocomplete="off" placeholder="再次输入登录密码" name="newpassword2" id="newpassword2">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-3">
                            <button class="btn btn-primary" type="submit">提交</button>
                        </div>
                    </div>
                 </form>
        	</div>
        </div>
		<script type="application/javascript">
			require(['jquery', 'layer', 'jquery.validate.methods', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
				//初始化layer
				layer.config({
					path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
				});
				
				var err = "<i class='fa fa-times-circle'></i> ";
				
				$("#from1").validate({
					rules: {
						newpassword: {
							required: true,
							minlength: 6,
							maxlength: 30,
						},
						newpassword2: {
							required: true,
							minlength: 6,
							maxlength: 30,
							equalTo: '#newpassword'
						}
					},
					messages: {
						newpassword: {
							required: '密码必填'
						},
						newpassword2: {
							required: '密码必填',
							equalTo: '两次输入的密码必须要相同哟！'
						}
					},
					highlight: function(element) {
						$(element).closest(".form-group").removeClass("has-success").addClass("has-error")
					},
					success: function(element) {
						element.closest(".form-group").removeClass("has-error").addClass("has-success")
					},
					errorElement: "span",
					errorPlacement: function(element, r) {
						element.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
					},
					errorClass: "help-block m-b-none",
					validClass: "help-block m-b-none",
					onkeyup:false,
					submitHandler:function(form){
						return false;
						/* $(form).ajaxSubmit();
						var index = parent.layer.getFrameIndex(window.name);
						parent.$('.btn-refresh').click();
						parent.layer.close(index); */
					}
				});
				
			});
		</script>
	</body>
</html>