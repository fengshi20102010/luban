<!DOCTYPE HTML>
<html xmlns:th="http://www.thymeleaf.org">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>鲁班 - 用户列表</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="/ie" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link th:href="@{/components/bootstrap/3.3.7/css/bootstrap.min.css}" rel="stylesheet">
        <link th:href="@{/components/font-awesome/4.6.3/css/font-awesome.min.css}" rel="stylesheet">
        <link th:href="@{/css/animate.min.css}" rel="stylesheet">
        <link th:href="@{/css/style.css}" rel="stylesheet">
    </head>
    
    <body class="gray-bg">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>系统用户列表</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0);">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li>
                            <a href="javascript:void(0);">选项1</a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">选项2</a>
                        </li>
                    </ul>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <div class="row row-lg">
                    <!-- 查询条件 -->
                    <div class="well">
                        <h3>搜索</h3>
                        <form id="searchForm" class="form-inline">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">用户名</span>
                                    <input class="form-control" name="username" id="username" type="text" maxlength="50" value="${param.username}">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">昵称</span>
                                    <input class="form-control" name="nickname" id="nickname" type="text" maxlength="50" value="${param.nickname}">
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">用户类型</span>
                                    <select name="type" id="type" class="form-control">
                                        <option value="">所有</option>
                                        <option value="1" <c:if test="${param.type eq '1'}"> selected="selected"</c:if>>管理员</option>
                                        <option value="2" <c:if test="${param.type eq '2'}"> selected="selected"</c:if>>操作员</option>
                                    </select>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">状态</span>
                                    <select name="status" id="status" class="form-control">
                                        <option value="">所有</option>
                                        <option value="1" <c:if test="${param.status eq '1'}"> selected="selected"</c:if>>启用</option>
                                        <option value="0" <c:if test="${param.status eq '0'}"> selected="selected"</c:if>>禁用</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" id="pageSize" name="pageSize">
                                    <input type="hidden" id="pageNumber" name="pageNumber">
                                    <input type="button" class="btn btn-primary" id="search" value="搜索" style="margin-bottom:0">
                                </div>
                            </div>
                        </form>
                        <br>
                    </div>
                    <div class="col-sm-12">
                        <div class="btn-group hidden-xs" id="toolbar" role="group">
                            <button type="button" class="btn btn-outline btn-default" id="add">
                                <i class="glyphicon glyphicon-plus" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-outline btn-default">
                                <i class="glyphicon glyphicon-heart" aria-hidden="true"></i>
                            </button>
                            <button type="button" class="btn btn-outline btn-default">
                                <i class="glyphicon glyphicon-trash" aria-hidden="true"></i>
                            </button>
                        </div>
                        <table id="bootstrap-table" data-mobile-responsive="true">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>id</th>
                                    <th>用户名</th>
                                    <th>昵称</th>
                                    <th>角色</th>
                                    <th>用户类型</th>
                                    <th>邮箱</th>
                                    <th>状态</th>
                                    <th>用户描述</th>
                                    <th>创建时间</th>
                                    <th>最后登陆ip</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" th:src="@{/js/require.min.js}"></script>
        <script type="text/javascript" th:src="@{/js/config.js}"></script>
        <script>
            require(['bootstrap-table.zh-CN', 'layer', 'moment', 'contabs.min', 'content.min', 'main.min'], function($, layer, moment) {
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
                
                $('#bootstrap-table').bootstrapTable({
                    contentType: "application/x-www-form-urlencoded",
                    method: 'post',
                    toolbar: '#toolbar',
                    iconSize: "outline",
                    icons: {
                        refresh: "glyphicon-repeat",
                        toggle: "glyphicon-list-alt",
                        columns: "glyphicon-list"
                    },
                    striped: true,
                    cache: false,
                    pagination: true,
                    sortable: false,
                    pageNumber: 1,
                    pageSize: 10,
                    pageList: [10, 25, 50, 100],
                    url: window.location.href,
                    queryParamsType: '',
                    queryParams: function(params){
                        $('#pageNumber').val(params.pageNumber);
                        $('#pageSize').val(params.pageSize);
                        return $('#searchForm').serialize();
                    },
                    sidePagination: "server", 
                    strictSearch: true,
                    showColumns: true, 
                    showRefresh: true, 
                    minimumCountColumns: 2, 
                    searchOnEnterKey: true,
                    pagination: true,
                    columns: [
                        {
                            checkbox: true,
                            align: 'center'
                        }, 
                        {
                            field: 'id',
                            align: 'center'
                        }, 
                        {
                            field: 'username',
                            align: 'center'
                        }, 
                        {
                            field: 'nickname',
                            align: 'center'
                        }, 
                        {
                            field: 'role.roleName',
                            align: 'center'
                        }, 
                        {
                            field: 'type',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '';
                            	if(value == 1){
                                    content = '<span class="label label-info">管理员</span>';
                            	}
                            	if(value == 2){
                                    content = '<span class="label label-info">操作员</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'email',
                            align: 'center'
                        }, 
                        {
                            field: 'status',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	var content = '';
                            	if(value == 1){
                                    content = '<span class="label label-info">启用</span>';
                            	}
                            	if(value == 0){
                                    content = '<span class="label label-info">禁用</span>';
                            	}
                            	return content;
                            }
                        }, 
                        {
                            field: 'description',
                            align: 'center'
                        }, 
                        {
                            field: 'createTime',
                            align: 'center',
                            formatter: function(value, row, index) {
                            	return moment(value).format('YYYY-MM-DD');
                            }
                        }, 
                        {
                            field: 'lastLoginIp',
                            align: 'center'
                        }, 
                        {
                            field: 'id',
                            align: 'center',
                            formatter: function(value, row, index){
                                var content = '';
                                
                                <security:hasPermission name="sys:user:view">
                                    content += '<a title="详情" href="javascript:;" class="ml-5 view" style="text-decoration:none"><i class="fa fa-eye"></i></a>';
                                </security:hasPermission>
                                <security:hasPermission name="sys:user:update">
                                    content += '<a title="编辑" href="javascript:;" class="ml-5 update" style="text-decoration:none" ><i class="fa fa-edit"></i></a>';
                                </security:hasPermission>
								<security:hasPermission name="sys:user:status">
	                               	content += '<a title="修改状态" href="javascript:;" class="ml-5 status" status="'+value+'"style="text-decoration:none">'
	                               	content +=	value == 1 ? '<i class="Hui-iconfont"></i>':'<i class="Hui-iconfont"></i>'
	                               	content +=	'</a>';
								</security:hasPermission>
						        <security:hasPermission name="sys:user:pass">
	                               	content += '<a title="重置密码" href="javascript:;" class="ml-5 pass" style="text-decoration:none"><i class="fa fa-key"></i></a>';
	                            </security:hasPermission>
                                if('' == content) content = '无权限';
                                return content;
                            }
                        }
                    ]
                });

                // 搜索
                $('#search').on('click', function() {
                    $('#bootstrap-table').bootstrapTable('refresh');
                });
                
                // 新增
                $('#add').on('click', function() {
                    var title = '新增系统用户表';
                    var url = 'add.html';
                    layerShow(title, url, "800", "500");
                });

                // 编辑
                $('#bootstrap-table').delegate('.update', 'click', function() {
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    var title = '编辑系统用户表';
                    var url = 'update.html?id=' + id;
                    layerShow(title, url, 800, 500);
                });

                // 查看
                $('#bootstrap-table').delegate('.view', 'click', function() {
                    var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    var title = '查看系统用户';
                    var url = 'view.html?id=' + id;
                    layerShow(title, url);
                });
                
                // 重置密码
				$('#bootstrap-table').delegate('.pass', 'click', function() {
					var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
					var title = '重置密码';
					var url = 'pass.html?id=' + id;
					layerShow(title,url,null,300);
				});

				// 修改状态
				$('#bootstrap-table').delegate('.status', 'click', function() {
					var $this = $(this);
					var index = $(this).parents('tr').data('index');
                    var id = $('#bootstrap-table').bootstrapTable('getData')[index].id;
                    var status = $('#bootstrap-table').bootstrapTable('getData')[index].status;
					layer.confirm(status == 0 ? '确认要启用吗？':'确认要停用吗？',function(index){
						$.post('status.html', {"id": id, "status": $this.attr('status') == 1 ? 0 : 1}, function(d){
							if(d.success){
								layer.msg('操作成功！', {icon: 1, time: 1000});
                                setTimeout($('#bootstrap-table').bootstrapTable('refresh'), 1000);
                                return;
							}
							layer.msg(d.message, {icon: 5, time: 1000});
						},"json");
					});
				});
            });
        </script>
    </body>
</html>