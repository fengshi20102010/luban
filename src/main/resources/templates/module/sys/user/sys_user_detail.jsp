<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title>魔镜助手 - 系统用户新增/修改</title>
        <!--[if lt IE 9]>
            <meta http-equiv="refresh" content="0;ie.html" />
        <![endif]-->
        <link rel="shortcut icon" href="favicon.ico">
        <link href="${pageContext.request.contextPath }/resource/components/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/components/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/animate.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath }/resource/css/style.min.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/config.js"></script>
    </head>
    <body>
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal m-t" id="form1">
                    <c:if test="${not empty item}">
                        <input type="hidden" name="id" value="${item.id}" />
                    </c:if>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">用户名：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="username" name="username" placeholder="请输入用户名"  value="${item.username}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">昵称：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="nickname" name="nickname" placeholder="请输入昵称"  value="${item.nickname}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <c:if test="${empty item}">
	                     <div class="form-group">
	                         <label class="col-sm-3 control-label">初始密码：</label>
	                         <div class="col-sm-8">
	                             <input type="password" class="form-control" autocomplete="off" value="" placeholder="密码" id="password" name="password">
	                         </div>
	                     </div>
	                     <div class="form-group">
	                         <label class="col-sm-3 control-label">确认密码：</label>
	                         <div class="col-sm-8">
	                             <input type="password" class="form-control" autocomplete="off"  placeholder="确认新密码" id="password2" name="password2">
	                         </div>
	                     </div>
					</c:if>
					<div class="form-group">
                        <label class="col-sm-3 control-label">邮箱：</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="email" name="email" placeholder="请输入邮箱"  value="${item.email}">
                                <!-- <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 这里写点提示的内容</span> -->
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">用户类型：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="type" id="type1" value="1"> <i></i> 管理员
                                </label>
                                <label>
                                    <input type="radio" name="type" id="type2" value="2"> <i></i> 操作员
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("type${empty item ? 1 : item.type}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">状态：</label>
                            <div class="radio i-checks">
                                <label>
                                    <input type="radio" name="status" id="status1" value="1"> <i></i> 启用
                                </label>
                                <label>
                                    <input type="radio" name="status" id="status0" value="0"> <i></i> 禁用
                                </label>
                                <script type="text/javascript">
                                    document.getElementById("status${empty item ? 1 : item.status}").checked=true;
                                </script>
                            </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">角色：</label>
                        <div class="col-sm-4">
                            <select class="form-control m-b" name="roleId" value="${item.role.id}" size="1">
                                <c:forEach items="${roles }" var="role">
		                            <option value="${role.id }" ${role.id eq item.role.id ? 'selected':''}>${role.roleName }</option>
		                        </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">备注：</label>
                        <div class="col-sm-8">
                            <textarea name="description" id="description" class="form-control" placeholder="说点什么...100个字符以内" dragonfly="true">${item.description}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-3">
                            <button class="btn btn-primary" type="submit">提交</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        <script>
            require(['jquery', 'layer', 'jquery.validate.methods', 'jquery.icheck', 'contabs.min', 'content.min', 'main.min'], function($, layer) {
                //初始化layer
                layer.config({
                    path: '${pageContext.request.contextPath }/resource/components/layer/3.0.1/'
                });
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '20%'
                });
                var err = "<i class='fa fa-times-circle'></i> ";
                $("#form1").validate({
                    rules: {
                            username : {
                                required: true
                            },
                            <c:if test="${empty item}">
							password:{
								required:true,
							},
							password2:{
								required:true,
								equalTo: "#password"
							},
							</c:if>
                            email : {
                                required: true
                            }
                    },
                    messages: {
                        username : {
                            required: err + ' '
                        },
                        <c:if test="${empty item}">
						password:{
							required: err + '请输入登陆密码',
						},
						password2:{
							required: err + '请再次输入登陆密码',
							equalTo:  err + '两次输入的密码不一致',
						},
						</c:if>
                        email : {
                            required: err + ' '
                        }
                    },
                    highlight: function(element) {
                        $(element).closest(".form-group").removeClass("has-success").addClass("has-error")
                    },
                    success: function(element) {
                        element.closest(".form-group").removeClass("has-error").addClass("has-success")
                    },
                    errorElement: "span",
                    errorPlacement: function(element, r) {
                        element.appendTo(r.is(":radio") || r.is(":checkbox") ? r.parent().parent().parent() : r.parent())
                    },
                    errorClass: "help-block m-b-none",
                    validClass: "help-block m-b-none",
                    onkeyup:false,
                    submitHandler:function(form){
                        $.ajax({
                            url:window.location.href,
                            dataType:'json',
                            type:'post',
                            data: $('#form1').serialize(),
                            success:function(data){
                                if(data.success){
                                    var index = parent.layer.getFrameIndex(window.name);
                                    parent.$('#search').click();
                                    parent.layer.close(index);
                                    return;
                                }else{
                                    layer.msg(data.message,{icon:2,time:1000});
                                }
                            }
                        })
                    }
                });
            });
        </script>
    </body>
</html>