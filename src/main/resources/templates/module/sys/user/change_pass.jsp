<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="renderer" content="webkit|ie-comp|ie-stand">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
		<meta http-equiv="Cache-Control" content="no-siteapp" />
		<LINK rel="Bookmark" href="/favicon.ico">
		<LINK rel="Shortcut Icon" href="/favicon.ico" />
		<!--[if lt IE 9]>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/html5.js"></script>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/respond.min.js"></script>
			<script type="text/javascript" src="${pageContext.request.contextPath }/resource/components/PIE_IE678.js"></script>
		<![endif]-->
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/H-ui.min.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/H-ui.admin.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/components/Hui-iconfont/1.0.7/iconfont.css" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/resource/css/style.css" />
		<!--[if IE 6]>
			<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
			<script>DD_belatedPNG.fix('*');</script>
		<![endif]-->
		<title>增加系统用户</title>
	</head>
	
	<body>
		<article class="page-container">
			<form class="form form-horizontal" method="post" action="change_pass.html" id="form1">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-3">登录用户名：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text" class="input-text" id="username" name="username" placeholder="请输入登录用户名" readonly value="<security:principal />" >
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>当前登录密码</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="password" class="input-text" autocomplete="off" value="" placeholder="当前登录密码" id="oldpassword" name="oldpassword">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>新登录密码：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="password" class="input-text" autocomplete="off"  placeholder="新登录密码" name="newpassword" id="newpassword">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>再次输入新登录密码：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="password" class="input-text" autocomplete="off"  placeholder="再次输入登录密码" name="newpassword2" id="newpassword2">
					</div>
				</div>
				<div class="row cl">
					<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
						<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;修改密码&nbsp;&nbsp;">
					</div>
				</div>
			</form>
		</article>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/require.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/app/config.js"></script>
		<script type="text/javascript">
			require(['jquery', 'layer', 'jquery.validate', 'hui.admin'],function($, layer){
				//初始化layer
				layer.config({
					path: '${pageContext.request.contextPath }/resource/components/layer/2.2/'
				});
				
				$("#form1").validate({
					rules: {
						oldpassword: {
							required: true,
							maxlength: 30
						},
						newpassword: {
							required: true,
							maxlength: 30,
							notEqualTo: '#oldpassword'
						},
						newpassword2: {
							required: true,
							maxlength: 30,
							equalTo: '#newpassword'
						}
					},
					messages: {
						oldpassword: {
							required: ' ',
							maxlength: '最多只能输入{0}个字符'
						},
						newpassword: {
							required: ' ',
							maxlength: '最多只能输入{0}个字符',
							notEqualTo: '新密码和旧密码不能相同！'
						},
						newpassword2: {
							required: ' ',
							maxlength: '最多只能输入{0}个字符',
							equalTo: '两次输入的密码必须要相同哟！'
						}
					},
					onkeyup:false,
					focusCleanup:true,
					success:"valid",
					submitHandler:function(form){
						/* $(form).ajaxSubmit();
						var index = parent.layer.getFrameIndex(window.name);
						parent.$('.btn-refresh').click();
						parent.layer.close(index); */
					}
				});
			})
		</script>
	</body>
</html>