package com.fs.luban.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @title Swagger2 配置
 * @author fengshi
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	public static final String TITLE = "鲁班 RESTful APIs";
	public static final String DESCRIPTION = "鲁班 restful 接口文档";
	public static final String VERSION = "0.0.1";
	public static final String URL = "https://gitee.com/fengshi20102010/luban";
	
	public static final String AUTHOR_NAME = "鲁班";
	public static final String AUTHOR_URL = "https://gitee.com/fengshi20102010";
	public static final String AUTHOR_EMAIL = "fengshi20102010@qq.com";

	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.fs.luban.module.*.controller"))
				.paths(PathSelectors.any())
				.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title(TITLE)
				.description(DESCRIPTION)
				.termsOfServiceUrl(URL)
				.version(VERSION)
				.contact(new Contact(AUTHOR_NAME, AUTHOR_URL, AUTHOR_EMAIL))
				.build();
	}

}
