package com.fs.luban.module.sys.controller;

import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

import com.fs.luban.common.controller.CommonController;
import com.fs.luban.common.support.JsonListResult;
import com.fs.luban.common.support.JsonResult;
import com.fs.luban.module.sys.entity.SysAttach;
import com.fs.luban.module.sys.service.SysAttachService;
import com.fs.luban.security.SessionFace;
import com.fs.luban.security.model.LubanUserDetails;
import com.google.common.collect.Lists;

/**
 * <p>
 * 系统_附件表 前端控制器
 * </p>
 *
 * @author fengshi
 * @since 2018-05-16
 */
@RestController
@RequestMapping("global")
public class SysAttachController extends CommonController {

	@Resource
	private SysAttachService attachService;

	@PostMapping
	public JsonListResult<SysAttach> upload(HttpServletRequest request, HttpServletResponse response) {
		JsonListResult<SysAttach> result = new JsonListResult<SysAttach>();
		// 上传附件需要合法用户（本平台登录用户）
		if (!SessionFace.isUserLogin()) {
			result.setSuccess(false);
			result.setCode("-1");
			result.setMessage("非法操作");
			return result;
		}
		LubanUserDetails user = SessionFace.getSessionUser();
		String sysKey = "sys";
		// 转换附件对象
		CommonsMultipartResolver mutilpartResolver = new CommonsMultipartResolver(
				request.getSession().getServletContext());
		List<SysAttach> list = Lists.newArrayList();
		// request如果是Multipart类型
		if (mutilpartResolver.isMultipart(request)) {
			// 强转成 MultipartHttpServletRequest
			MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest) request;
			// 获取MultipartFile类型文件
			Iterator<String> it = multiRequest.getFileNames();
			while (it != null && it.hasNext()) {
				MultipartFile fileDetail = multiRequest.getFile(it.next());
				if (fileDetail != null) {
					SysAttach attach = attachService.upload(user.getId(), sysKey, "sys", fileDetail);
					list.add(attach);
				}
			}
		}
		if (CollectionUtils.isEmpty(list)) {
			result.setSuccess(false);
			result.setCode("-1");
			result.setMessage("未发现附件");
			return result;
		}
		result.setRows(list);
		result.setCode("操作成功");
		result.setTotal(Long.valueOf(list.size()));
		return result;
	}

	@DeleteMapping
	public JsonResult delete(HttpServletRequest request, @RequestParam(value = "url", required = true) String url) {
		// 删除附件只能删除自己的附件，必须登录
		if (!SessionFace.isUserLogin()) {
			return operFailure("非法操作", "-1");
		}
		try {
			if (url.indexOf(",") > 0) {
				for (String u : url.split(",")) {
					attachService.delete(SessionFace.getSessionUser().getId(), u);
				}
			} else {
				attachService.delete(SessionFace.getSessionUser().getId(), url);
			}
			return operSuccess();
		} catch (Exception e) {
			return operFailure("删除失败！", "-1");
		}
	}

}
