package com.fs.luban.module.sys.entity.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @title 排序
 * @author fengshi
 */
@Data
public class SortInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	/** id */
	private Integer id;

	/** pid */
	private Integer pid;

	/** 排序号 */
	private Integer sortNo;

	/** 子排序信息 */
	private List<SortInfo> children;
	
}
