package com.fs.luban.module.sys.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fs.luban.module.sys.entity.SysResource;
import com.fs.luban.module.sys.entity.dto.SortInfo;
import com.fs.luban.module.sys.entity.vo.SysResourceVO;

/**
 * <p>
 * 系统-资源表 服务类
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
public interface SysResourceService extends IService<SysResource> {

	/**
	 * @title 查询全部资源树
	 * @author fengshi
	 */
	List<SysResourceVO> queryResList();

	/**
	 * @title 根据id删除资源，并一并删除子资源
	 * @author fengshi
	 */
	Boolean deleteTree(Integer id);

	/**
	 * @title 根据列表信息排序
	 * @author fengshi
	 */
	Boolean sort(List<SortInfo> sortInfo);

	/**
	 * @title 查询所有状态正常的资源
	 * @return List<SysResource>
	 * @author fengshi
	 */
	List<SysResource> queryAllRes();

}
