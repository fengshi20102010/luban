package com.fs.luban.module.sys.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fs.luban.module.sys.entity.SysRoleResource;
import com.fs.luban.module.sys.mapper.SysRoleResourceMapper;
import com.fs.luban.module.sys.service.SysRoleResourceService;

/**
 * <p>
 * 系统-角色资源映射表 服务实现类
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Service
public class SysRoleResourceServiceImpl extends ServiceImpl<SysRoleResourceMapper, SysRoleResource> implements SysRoleResourceService {

}
