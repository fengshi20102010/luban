package com.fs.luban.module.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fs.luban.module.sys.entity.SysOrg;
import com.fs.luban.module.sys.entity.vo.SysOrgVO;
import com.fs.luban.module.sys.mapper.SysOrgMapper;
import com.fs.luban.module.sys.service.SysOrgService;

/**
 * <p>
 * 系统-组织表 服务实现类
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Service
public class SysOrgServiceImpl extends ServiceImpl<SysOrgMapper, SysOrg> implements SysOrgService {


    @Autowired
    private SysOrgMapper sysOrgMapper;

    /**
     * 获取树
     *
     * @return
     */
    @Override
    public List<SysOrgVO> getListTree() {
        return sysOrgMapper.getListByPid(0);
    }
}
