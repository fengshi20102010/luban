package com.fs.luban.module.sys.service.impl;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fs.luban.common.Constant;
import com.fs.luban.module.sys.entity.SysResource;
import com.fs.luban.module.sys.entity.dto.SortInfo;
import com.fs.luban.module.sys.entity.vo.SysResourceVO;
import com.fs.luban.module.sys.mapper.SysResourceMapper;
import com.fs.luban.module.sys.service.SysResourceService;
import com.google.common.collect.Lists;

/**
 * <p>
 * 系统-资源表 服务实现类
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Service
public class SysResourceServiceImpl extends ServiceImpl<SysResourceMapper, SysResource> implements SysResourceService {

	@Autowired
	private SysResourceMapper sysResourceMapper;

	@Override
	public List<SysResourceVO> queryResList() {
		return sysResourceMapper.queryResList(0);
	}

	@Override
	@Transactional
	public Boolean deleteTree(Integer id) {
		// 获取该资源极其子资源
		SysResourceVO resVO = getTreeById(id);
		if(null != resVO){
			// 获取所有节点id
			List<Integer> ids = getIds(resVO);
			// 批量删除资源
			if(!CollectionUtils.isEmpty(ids)){
				return sysResourceMapper.deleteBatchIds(ids) > 0;
			}
		}
		return false;
	}

	@Override
	@Transactional
	public Boolean sort(List<SortInfo> sortInfo) {
		if(!CollectionUtils.isEmpty(sortInfo)){
			for (int i = 0; i < sortInfo.size(); i++) {
				sysResourceMapper.sort(sortInfo.get(i).getId(), sortInfo.get(i).getPid(), i + 1);
				if(!CollectionUtils.isEmpty(sortInfo.get(i).getChildren())){
					sort(sortInfo.get(i).getChildren());
				}
			}
			return true;
		}
		return false;
	}

	@Override
	public List<SysResource> queryAllRes() {
		// 查询条件为所有状态正常的受保护资源
		SysResource res = new SysResource();
		res.setStatus(Constant.Global.State.Enable.getValue());
		QueryWrapper<SysResource> wrapper = new QueryWrapper<SysResource>(res);
		wrapper.orderByDesc(" id ");
		return list(wrapper);
	}
	
	private SysResourceVO getTreeById(Integer id){
		// 查询出该节点
		SysResource res = sysResourceMapper.selectById(id);
		if(null != res){
			SysResourceVO resVO = new SysResourceVO();
			BeanUtils.copyProperties(res, resVO);
			// 查询子节点
			resVO.setChildren(sysResourceMapper.queryResList(id));
			return resVO;
		}
		return null;
	}
	
	// 获取所有资源节点的id
	private List<Integer> getIds(SysResourceVO resVO){
		if(null == resVO){
			return null;
		}
		List<Integer> ids = Lists.newArrayList();
		ids.add(resVO.getId());
		if(!CollectionUtils.isEmpty(resVO.getChildren())){
			ids.addAll(getChildrenIds(resVO.getChildren()));
		}
		return ids;
	}
	
	// 获取所有子资源节点的id
	private List<Integer> getChildrenIds(List<SysResourceVO> resList) {
		List<Integer> ids = Lists.newArrayList();
		if(!CollectionUtils.isEmpty(resList)){
			for (SysResourceVO res : resList) {
				ids.add(res.getId());
				if(!CollectionUtils.isEmpty(res.getChildren())){
					ids.addAll(getChildrenIds(res.getChildren()));
				}
			}
		}
		return ids;
	}
	
}
