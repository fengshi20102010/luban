package com.fs.luban.module.sys.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fs.luban.module.sys.entity.SysOlog;
import com.fs.luban.module.sys.mapper.SysOlogMapper;
import com.fs.luban.module.sys.service.SysOlogService;

/**
 * <p>
 * 系统-操作日志表 服务实现类
 * </p>
 *
 * @author fengshi
 * @since 2018-05-10
 */
@Service
public class SysOlogServiceImapl extends ServiceImpl<SysOlogMapper, SysOlog> implements SysOlogService {

}
