package com.fs.luban.module.sys.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统-资源表
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_resource")
public class SysResource extends Model<SysResource> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 父id
	 */
	private Integer pid;
	/**
	 * 资源名称
	 */
	private String name;
	/**
	 * 资源地址
	 */
	private String url;
	/**
	 * 资源类型{1:菜单,2:按钮,3:其他}
	 */
	private Integer type;
	/**
	 * 资源状态{0:禁用,1:启用}
	 */
	private Integer status;
	/**
	 * 资源请求类型{0:all,1:get,2:post,3:put,4:delete,5:other}
	 */
	private Integer method;
	/**
	 * 是否展示{0:隐藏,1:展示}
	 */
	@TableField("is_show")
	private Integer isShow;
	/**
	 * 权限标志
	 */
	private String permission;
	/**
	 * 图标
	 */
	private String icon;
	/**
	 * 排序
	 */
	@TableField("sort_no")
	private Integer sortNo;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 修改时间
	 */
	@TableField("update_time")
	private Date updateTime;
	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;
	/**
	 * 乐观锁
	 */
	@Version
	private Integer version;

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
