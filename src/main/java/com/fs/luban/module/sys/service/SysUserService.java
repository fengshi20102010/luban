package com.fs.luban.module.sys.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.fs.luban.module.sys.entity.SysUser;
import com.fs.luban.module.sys.entity.vo.SysUserDetailVO;
import com.fs.luban.module.sys.entity.vo.SysUserVO;

/**
 * <p>
 * 系统-用户表 服务类
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
public interface SysUserService extends IService<SysUser> {
	
	/**
	 * @title 根据用户名查询用户
	 * @Description 根据用户名查询用户
	 * @param @param username
	 * @param @return   
	 * @return SysUser  
	 * @author fengshi
	 */
	SysUserDetailVO selectUserByUsername(String username);
	
	/**
	 * @title 分页查询系统用户列表
	 * @Description 分页查询系统用户列表
	 * @param @param page
	 * @param @return   
	 * @return Page<SysUserVO>  
	 * @author fengshi
	 */
	Page<SysUserVO> selectSysUserVOPage(Page<SysUserVO> page);
	
	/**
	 * @title 根据id查询系统用户信息
	 * @Description 根据id查询系统用户信息
	 * @param @param id
	 * @param @return   
	 * @return SysUserVO  
	 * @author fengshi
	 */
	SysUserVO selectSysUserVOById(Integer id);

	/**
	 * @title 检查用户名是否重复
	 * @Description 检查用户名是否重复
	 * @param @param username
	 * @param @return   
	 * @return Boolean  
	 * @author fengshi
	 */
	Boolean checkUsername(String username);
	
}
