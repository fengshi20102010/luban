package com.fs.luban.module.sys.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fs.luban.common.controller.CommonController;
import com.fs.luban.common.support.JsonResult;
import com.fs.luban.module.sys.entity.SysResource;
import com.fs.luban.module.sys.entity.dto.SortInfo;
import com.fs.luban.module.sys.service.SysResourceService;

/**
 * <p>
 * 系统-资源表 前端控制器
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Controller
@RequestMapping("/sys/sysResource")
public class SysResourceController extends CommonController {

	// 列表
	public static final String LIST = "module/sys/resource/sys_resource_list";
	// 新增、编辑
	public static final String EDIT = "module/sys/resource/sys_resource_edit";
	// 详情
	public static final String VIEW = "module/sys/resource/sys_resource_view";

	@Autowired
	private SysResourceService sysResourceService;

	// 列表
	@GetMapping("list")
	public String list(ModelMap map) {
		map.put("resList", sysResourceService.queryResList());
		return LIST;
	}

	// 新增-查看
	@GetMapping("add")
	public String add(@RequestParam Integer pid, @RequestParam Integer sort, ModelMap map) {
		map.put("pid", pid);
		map.put("sort", sort);
		return EDIT;
	}

	// 新增-保存
	@ResponseBody
	@PostMapping("add")
	public JsonResult ajaxAdd(SysResource sysResource) {
		if(sysResourceService.save(sysResource)){
			return operSuccess();
		}
		return operFailure("新增资源失败", "-1");
	}

	// 编辑-查看
	@GetMapping("edit")
	public String edit(@RequestParam Integer id, ModelMap map) {
		// 编辑
		map.put("item", sysResourceService.getById(id));
		return EDIT;
	}

	// 编辑-保存
	@ResponseBody
	@PostMapping("edit")
	public JsonResult ajaxEdit(SysResource sysResource) {
		if(sysResourceService.updateById(sysResource)){
			return operSuccess();
		}
		return operFailure("编辑资源失败", "-1");
	}

	// 查看
	@GetMapping("view")
	public String view(@RequestParam Integer id, ModelMap map) {
		map.put("item", sysResourceService.getById(id));
		return VIEW;
	}
	
    // 删除
    @ResponseBody
    @PostMapping("del")
    public JsonResult ajaxDel(@RequestParam Integer id){
		if(sysResourceService.deleteTree(id)){
			return operSuccess();
		}
		return operFailure("删除资源失败", "-1");
    }
	
    // 排序
    @ResponseBody
    @PostMapping("sort")
    public JsonResult ajaxSort(@RequestBody List<SortInfo> sortInfo){
		if(sysResourceService.sort(sortInfo)){
			return operSuccess();
		}
		return operFailure("排序失败", "-1");
    }
	
}
