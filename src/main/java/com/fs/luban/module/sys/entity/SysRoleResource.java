package com.fs.luban.module.sys.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统-角色资源映射表
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_role_resource")
public class SysRoleResource extends Model<SysRoleResource> {

	private static final long serialVersionUID = 1L;

	/**
	 * 角色id
	 */
	@TableId("role_id")
	private Integer roleId;
	/**
	 * 资源id
	 */
	@TableField("resource_id")
	private Integer resourceId;

	@Override
	protected Serializable pkVal() {
		return this.roleId + this.resourceId;
	}
}
