package com.fs.luban.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fs.luban.module.sys.entity.SysDict;

/**
 * <p>
 * 系统-数据字典表 Mapper 接口
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
