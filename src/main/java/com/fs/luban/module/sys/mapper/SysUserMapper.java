package com.fs.luban.module.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fs.luban.module.sys.entity.SysUser;
import com.fs.luban.module.sys.entity.vo.SysUserDetailVO;
import com.fs.luban.module.sys.entity.vo.SysUserVO;

/**
 * <p>
 * 系统-用户表 Mapper 接口
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
public interface SysUserMapper extends BaseMapper<SysUser> {
	
	/**
	 * @title 查询用户列表
	 * @param @return   
	 * @return List<SysUserVO>  
	 * @author fengshi
	 */
	List<SysUserVO> selectUserVOList(Page<SysUserVO> page);

	/**
	 * @title 查询单个用户列表
	 * @param @param id
	 * @param @return   
	 * @return SysUserVO  
	 * @author fengshi
	 */
	SysUserVO selectSysUserVOById(@Param("id")Integer id);

	/**
	 * @title 根据名称查询用户信息
	 * @param @param username
	 * @param @return   
	 * @return SysUserDetailVO  
	 * @author fengshi
	 */
	SysUserDetailVO selectSysUserVOByUsername(@Param("username")String username);

}
