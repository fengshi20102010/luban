package com.fs.luban.module.sys.entity.vo;

import java.io.Serializable;

import lombok.Data;

@Data
public class SysUserDetailVO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * id
	 */
	private Integer id;
	/**
	 * 部门id
	 */
	private Integer orgId;
	/**
	 * 部门名称	
	 */
	private String orgName;
	/**
	 * 角色id
	 */
	private Integer roleId;
	/**
	 * 角色名称
	 */
	private String roleName;
	/**
	 * 账号
	 */
	private String username;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 性别{0:未知,1:男,2:女}
	 */
	private Integer sex;
	/**
	 * 电话
	 */
	private String mobile;
	/**
	 * 邮箱
	 */
	private String email;
	/**
	 * 头像
	 */
	private String avator;
	/**
	 * 状态{0:停用,1:正常,2:锁定}
	 */
	private Integer status;

}
