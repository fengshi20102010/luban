package com.fs.luban.module.sys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * 系统-操作日志表 前端控制器
 * </p>
 *
 * @author fengshi
 * @since 2018-05-10
 */
@Controller
@RequestMapping("/sys/sysOlog")
public class SysOlogController {

}
