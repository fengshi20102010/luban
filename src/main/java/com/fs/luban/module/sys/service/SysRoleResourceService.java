package com.fs.luban.module.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fs.luban.module.sys.entity.SysRoleResource;

/**
 * <p>
 * 系统-角色资源映射表 服务类
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
public interface SysRoleResourceService extends IService<SysRoleResource> {

}
