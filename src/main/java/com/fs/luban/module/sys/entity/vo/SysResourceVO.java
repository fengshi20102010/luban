package com.fs.luban.module.sys.entity.vo;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * 资源VO
 */
@Data
public class SysResourceVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private Integer id;

	/**
	 * 父id
	 */
	private Integer pid;

	/**
	 * 资源名称
	 */
	private String name;

	/**
	 * 资源地址
	 */
	private String url;

	/**
	 * 资源类型{1:菜单,2:按钮,3:其他}
	 */
	private Integer type;

	/**
	 * 资源状态{0:禁用,1:启用}
	 */
	private Integer status;

	/**
	 * 资源请求类型{0:all,1:get,2:post,3:put,4:delete,5:other}
	 */
	private Integer method;

	/**
	 * 是否展示{0:隐藏,1:展示}
	 */
	private Integer isShow;

	/**
	 * 权限标志
	 */
	private String permission;

	/**
	 * 图标
	 */
	private String icon;

	/**
	 * 排序
	 */
	private Integer sortNo;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 乐观锁
	 */
	private Integer version;

	/**
	 * 子节点
	 */
	private List<SysResourceVO> children;

}
