package com.fs.luban.module.sys.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fs.luban.common.controller.CommonController;
import com.fs.luban.common.support.JsonListResult;
import com.fs.luban.common.support.JsonResult;
import com.fs.luban.module.sys.entity.SysOrg;
import com.fs.luban.module.sys.entity.vo.SysOrgVO;
import com.fs.luban.module.sys.service.SysOrgService;

/**
 * <p>
 * 系统-组织表 前端控制器
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Controller
@RequestMapping("/sys/sysOrg")
public class SysOrgController extends CommonController {

    // 列表
    public static final String LIST = "module/sys/org/sys_org_list";
    // 编辑
    public static final String EDIT = "module/sys/org/sys_org_edit";
    // 新增
    public static final String ADD = "module/sys/org/sys_org_add";
    // 详情
    public static final String VIEW = "module/sys/org/sys_org_view";

    @Autowired
    private SysOrgService sysOrgService;

    // 列表
    @GetMapping("list")
    public String list() {
        return LIST;
    }

    // ajax列表
    @ResponseBody
    @PostMapping("getTreeList")
    public List<SysOrgVO> getTreeList() {
        return sysOrgService.getListTree();
    }


    // ajax列表
    @ResponseBody
    @PostMapping("list")
    public JsonListResult<SysOrg> ajaxList() {
        // 查询条件构造
        Wrapper<SysOrg> wrapper = new QueryWrapper<SysOrg>();
        return toJsonListResult(sysOrgService.page(getPage(), wrapper));
    }

    // 新增-查看
    @GetMapping("add")
    public String add(ModelMap map) {
        List<SysOrgVO> listTree = sysOrgService.getListTree();
        // 放入权限等相关的东西
        map.put("orgList", listTree);
        return ADD;
    }

    // 新增-保存
    @ResponseBody
    @PostMapping("add")
    public JsonResult ajaxAdd(SysOrg sysOrg, String isRadio) {
        if (sysOrg != null) {
            if ("0".equals(isRadio)) {
                sysOrg.setPid(0);
            }
            //检验全称是否存在
            SysOrg querySysOrg = new SysOrg();
            querySysOrg.setName(sysOrg.getName());
            Wrapper<SysOrg> wrapper = new QueryWrapper<>(querySysOrg);
            List<SysOrg> list = sysOrg.selectList(wrapper);
            if (list != null && list.size() > 0) {
                return operFailure("部门全称已存在!", "-1");
            } else {
                if (sysOrgService.save(sysOrg)) {
                    return operSuccess();
                }
            }
        }
        return operFailure("新增部门失败", "-1");
    }

    // 编辑-查看
    @GetMapping("edit")
    public String edit(@RequestParam Integer id, ModelMap map) {
        // 获取所有部门
        map.put("orgList", sysOrgService.getListTree());
        // 编辑
        map.put("item", sysOrgService.getById(id));
        // 放入权限等相关的东西
        return EDIT;
    }

    // 编辑-保存
    @ResponseBody
    @PostMapping("edit")
    public JsonResult ajaxEdit(SysOrg sysOrg, String isRadio) {
        if(sysOrg != null){
            if ("0".equals(isRadio)) {
                sysOrg.setPid(0);
            }
            if (sysOrgService.updateById(sysOrg)) {
                return operSuccess();
            }
        }
        return operFailure("新增部门失败", "-1");
    }

    // 查看
    @GetMapping("view")
    public String view(@RequestParam Integer id, ModelMap map) {
        map.put("item", sysOrgService.getById(id));
        return VIEW;
    }

    // 删除
    @ResponseBody
    @PostMapping("del")
    public JsonResult ajaxDel(@RequestParam Integer id) {
        if(id == null){
            return operFailure("请选择需要删除的部门", "-1");
        }
        //有子菜单不能被删除
        SysOrg org = new SysOrg();
        org.setPid(id);
        int count = sysOrgService.count(new QueryWrapper<>(org));
        if(count > 0){
            return operFailure("请先删除子部门", "-1");
        }
        if (sysOrgService.removeById(id)) {
            return operSuccess();
        }
        return operFailure("删除部门失败", "-1");
    }

}
