package com.fs.luban.module.sys.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fs.luban.module.sys.entity.SysAttach;
import com.fs.luban.module.sys.mapper.SysAttachMapper;
import com.fs.luban.module.sys.service.SysAttachService;

/**
 * <p>
 * 系统_附件表 服务实现类
 * </p>
 *
 * @author fengshi
 * @since 2018-05-16
 */
@Service
public class SysAttachServiceImapl extends ServiceImpl<SysAttachMapper, SysAttach>
		implements SysAttachService {

	@Value("${luban.attach.root}")
	private String attachRoot;

	public SysAttach getAttachment(Integer userId, String attachUrl) {
		SysAttach entity = new SysAttach();
		entity.setAttachUrl(attachUrl);
		if (null != userId) {
			entity.setUserId(userId);
		}
		Wrapper<SysAttach> wrapper = new QueryWrapper<SysAttach>(entity);
		List<SysAttach> list = list(wrapper);
		if (!CollectionUtils.isEmpty(list))
			return list.get(0);
		return null;
	}

	@Override
	public SysAttach delete(Integer userId, String oldAttachUrl) {
		SysAttach attachment = getAttachment(userId, oldAttachUrl);
		if (attachment == null) {
			return null;
		}
		File file = new File(attachment.getFilePath());
		if (file.exists() && file.isFile()) {
			final String fileName = file.getName().substring(0, file.getName().lastIndexOf("."));
			File[] files = file.getParentFile().listFiles(new FilenameFilter() {
				@Override
				public boolean accept(File dir, String name) {
					return name.indexOf(fileName) != -1;
				}
			});
			for (File f : files) {
				f.delete();
			}
		}
		removeById(attachment.getId());
		
		return attachment;
	}

	@Override
	public SysAttach delete(String oldAttachUrl) {
		return delete(null, oldAttachUrl);
	}

	@Override
	public SysAttach upload(Integer userId, String sysKey, String relationKey, MultipartFile file) {
		SysAttach attachment = new SysAttach();
		attachment.setUserId(userId);
		attachment.setRelationKey(relationKey);
		attachment.setSourceName(file.getOriginalFilename());
		attachment.setFileType(file.getContentType());
		attachment.setFileSize(file.getSize());
		// 文件名
		String fileName = UUID.randomUUID().toString().replaceAll("-", "");
		// 文件后缀
		String suffix = "";
		int len = file.getOriginalFilename().lastIndexOf(".");
		if (len >= 1) {
			suffix = file.getOriginalFilename().substring(len);
		}
		attachment.setFileName(fileName + suffix);
		// 组装保存路径-文件夹（按照关联模块区分）
		Calendar calendar = Calendar.getInstance();
		String path = String.format("/attach/%s/%s/%s/%s/%s", relationKey, calendar.get(Calendar.YEAR),
				calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DATE), attachment.getFileName());
		// 原始文件保存位置
		attachment.setFilePath(attachRoot + path);
		// 站点下的访问路径
		attachment.setAttachUrl(path);
		attachment.setCreateTime(new Date());
		File fileRoot = new File(attachment.getFilePath());

		if (fileRoot.getParentFile().isDirectory() || fileRoot.getParentFile().mkdirs()) {
			FileOutputStream fileOutputStream = null;
			try {
				fileOutputStream = new FileOutputStream(fileRoot);
				fileOutputStream.write(file.getBytes());
			} catch (Exception ex) {
				throw new RuntimeException(ex);
			} finally {
				if (fileOutputStream != null) {
					try {
						fileOutputStream.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		save(attachment);
		return attachment;
	}

	@Override
	public SysAttach upload(Integer userId, String sysKey, String relationKey, MultipartFile file,
			String oldAttachUrl) {
		if (StringUtils.isNotBlank(oldAttachUrl)) {
			delete(oldAttachUrl);
		}
		try {
			return upload(userId, sysKey, relationKey, file);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
