package com.fs.luban.module.sys.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fs.luban.common.controller.CommonController;

/**
 * <p>
 * 系统-数据字典表 前端控制器
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Controller
@RequestMapping("/sys/sysDict")
public class SysDictController extends CommonController {

}

