package com.fs.luban.module.sys.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fs.luban.module.sys.entity.SysDict;
import com.fs.luban.module.sys.mapper.SysDictMapper;
import com.fs.luban.module.sys.service.SysDictService;

/**
 * <p>
 * 系统-数据字典表 服务实现类
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */	
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService {

}
