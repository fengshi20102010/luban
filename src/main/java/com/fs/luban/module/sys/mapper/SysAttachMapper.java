package com.fs.luban.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fs.luban.module.sys.entity.SysAttach;

/**
 * <p>
 * 系统_附件表 Mapper 接口
 * </p>
 *
 * @author fengshi
 * @since 2018-05-16
 */
public interface SysAttachMapper extends BaseMapper<SysAttach> {

}
