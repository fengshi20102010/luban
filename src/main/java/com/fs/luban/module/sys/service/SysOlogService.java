package com.fs.luban.module.sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fs.luban.module.sys.entity.SysOlog;

/**
 * <p>
 * 系统-操作日志表 服务类
 * </p>
 *
 * @author fengshi
 * @since 2018-05-10
 */
public interface SysOlogService extends IService<SysOlog> {

}
