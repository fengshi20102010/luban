package com.fs.luban.module.sys.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fs.luban.module.sys.entity.SysOrg;
import com.fs.luban.module.sys.entity.vo.SysOrgVO;

/**
 * <p>
 * 系统-组织表 服务类
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
public interface SysOrgService extends IService<SysOrg> {

    /**
     * 获取树
     * @return
     */
    List<SysOrgVO> getListTree();

}
