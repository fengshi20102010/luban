package com.fs.luban.module.sys.service;

import org.springframework.web.multipart.MultipartFile;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fs.luban.module.sys.entity.SysAttach;

/**
 * <p>
 * 系统_附件表 服务类
 * </p>
 *
 * @author fengshi
 * @since 2018-05-16
 */
public interface SysAttachService extends IService<SysAttach> {

	/**
	 * 删除附件 会同时删除文件
	 * 
	 * @param oldAttachUrl
	 * @return
	 */
	public SysAttach delete(String oldAttachUrl);

	/**
	 * 删除附件 只能删除自己的文件
	 * 
	 * @param userId
	 * @param oldAttachUrl
	 * @return
	 */
	public SysAttach delete(Integer userId, String oldAttachUrl);

	/**
	 * 上传附件
	 * 
	 * @param userId
	 * @param sysKey
	 * @param relationKey
	 * @param file
	 * @return
	 */
	public SysAttach upload(Integer userId, String sysKey, String relationKey, MultipartFile file);

	/**
	 * 上传附件 用于替换时使用
	 * 
	 * @param userId
	 * @param sysKey
	 * @param relationKey
	 * @param file
	 * @param oldAttachUrl
	 * @return
	 */
	public SysAttach upload(Integer userId, String sysKey, String relationKey, MultipartFile file,
			String oldAttachUrl);

}
