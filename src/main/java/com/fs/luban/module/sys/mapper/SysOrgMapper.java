package com.fs.luban.module.sys.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fs.luban.module.sys.entity.SysOrg;
import com.fs.luban.module.sys.entity.vo.SysOrgVO;

/**
 * <p>
 * 系统-组织表 Mapper 接口
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
public interface SysOrgMapper extends BaseMapper<SysOrg> {

    /**
     * 根据父id获取list
     * @param pid
     * @return
     */
    List<SysOrgVO> getListByPid(Integer pid);

    /**
     * 根据id获取list，根目录不取
     * @param id
     * @return
     */
    List<SysOrg> getListWithOutParentPid(Integer id);

}
