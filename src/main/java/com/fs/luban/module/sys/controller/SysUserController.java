package com.fs.luban.module.sys.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fs.luban.common.controller.CommonController;
import com.fs.luban.common.support.JsonListResult;
import com.fs.luban.common.support.JsonResult;
import com.fs.luban.log.annotation.Olog;
import com.fs.luban.module.sys.entity.SysUser;
import com.fs.luban.module.sys.entity.vo.SysUserVO;
import com.fs.luban.module.sys.service.SysUserService;

/**
 * <p>
 * 系统-用户表 前端控制器
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Controller
@RequestMapping("/sys/sysUser")
public class SysUserController extends CommonController {

	// 列表
	private static final String LIST = "module/sys/user/sys_user_list";
	// 新增、编辑
	private static final String EDIT = "module/sys/user/sys_user_edit";
	// 详情
	private static final String VIEW = "module/sys/user/sys_user_view";
    // 修改密码
	private static final String CHANGE_PASS = "module/sys/user/sys_change_pass";
	
	private static final String DEFAULT_PASSWORD = "123456";

	@Autowired
	private SysUserService sysUserService;
	@Autowired
	private PasswordEncoder passwordEncoder;

	// 列表
	@Olog(name="用户列表")
	@GetMapping("list")
	public String list() {
		return LIST;
	}

	// ajax列表
	@Olog(name="ajax用户列表")
	@ResponseBody
	@PostMapping("list")
	public JsonListResult<SysUserVO> ajaxList(Map<String, Object> paramsMap) {
		// 查询条件构造
		Page<SysUserVO> page = getPage();
		return toJsonListResult(sysUserService.selectSysUserVOPage(page));
	}

	// 新增-查看
	@GetMapping("add")
	public String add(ModelMap map) {
		// 放入权限等相关的东西
		return EDIT;
	}

	// 新增-保存
	@ResponseBody
	@PostMapping("add")
	public JsonResult ajaxAdd(SysUser sysUser) {
		if(!sysUserService.checkUsername(sysUser.getUsername())){
			return operFailure("用户名已存在", "-1");
		}
		// 设置默认密码
		sysUser.setPassword(passwordEncoder.encode(DEFAULT_PASSWORD));
		if(sysUserService.save(sysUser)){
			return operSuccess();
		}
		return operFailure("新增用户失败", "-1");
	}

	// 编辑-查看
	@GetMapping("edit")
	public String edit(@RequestParam Integer id, ModelMap map) {
		// 编辑
		map.put("item", sysUserService.getById(id));
		// 放入权限等相关的东西
		return EDIT;
	}

	// 编辑-保存
	@ResponseBody
	@PostMapping("edit")
	public JsonResult ajaxEdit(SysUser sysUser) {
		if(sysUserService.updateById(sysUser)){
			return operSuccess();
		}
		return operFailure("新增用户失败", "-1");
	}

	// 查看
	@GetMapping("view")
	public String view(@RequestParam Integer id, ModelMap map) {
		map.put("item", sysUserService.selectSysUserVOById(id));
		return VIEW;
	}
	
    // 删除
    @ResponseBody
    @PostMapping("del")
    public JsonResult ajaxDel(@RequestParam Integer id){
		if(sysUserService.removeById(id)){
			return operSuccess();
		}
		return operFailure("删除用户失败", "-1");
    }
    
	// 重置密码-保存
    @ResponseBody
	@PostMapping("resetPass")
	public JsonResult resetPass(@RequestParam Integer id) {
		SysUser sysUser = getPwdUser(id);
		if (sysUser == null) {
			// 没有记录
			return operFailure("未找到该用户", "-1");
		}
		sysUser.setPassword(passwordEncoder.encode(DEFAULT_PASSWORD));
		if(sysUserService.updateById(sysUser)){
			return operSuccess();
		}
		return operFailure("重置密码失败", "-1");
	}
    
	// 修改密码-查看
	@GetMapping("changePass")
	public String changePass() {
		return CHANGE_PASS;
	}

	// 修改密码-保存
	@ResponseBody
	@PostMapping("changePass")
	public JsonResult changePassword(HttpServletRequest request, 
			@RequestParam("oldpassword") String orginalPassword,
			@RequestParam("newpassword") String password, 
			@RequestParam("newpassword2") String password2) {
//		SysUser sysUser = sysUserService.get(SessionFace.getSessionUser(request).getId());
		SysUser sysUser = getPwdUser(1);
		if (sysUser == null) {
			// 没有记录
			return operFailure("您尚未登陆，请登陆后在操作", "-1");
		} else {
			if(passwordEncoder.matches(orginalPassword, sysUser.getPassword())){
				if(password.equals(password2)){
					// 设置密码
					sysUser.setPassword(passwordEncoder.encode(password));
					sysUserService.updateById(sysUser);
					return operSuccess();
				} else {
					return operFailure("两次输入的密码不一致，请查证后再试", "-1");
				}
			}
			return operFailure("原始密码不正确，请确认并重试", "-1");
		}
	}

	// 检查用户名是否重复
	@ResponseBody
	@GetMapping("checkUsername")
	public Boolean checkUsername(@RequestParam String username) {
		return sysUserService.checkUsername(username);
	}
	
    // 只查询密码信息
    private SysUser getPwdUser(Integer id){
    	SysUser sysUser = new SysUser();
		sysUser.setId(id);
		QueryWrapper<SysUser> wrapper = new QueryWrapper<SysUser>(sysUser);
		wrapper.select("id", "password", "version");
		sysUser = sysUserService.getOne(wrapper);
		if(StringUtils.isEmpty(sysUser.getPassword())){
			return null;
		}
		return sysUser;
    }
	
}
