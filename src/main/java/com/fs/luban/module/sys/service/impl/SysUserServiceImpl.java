package com.fs.luban.module.sys.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fs.luban.module.sys.entity.SysUser;
import com.fs.luban.module.sys.entity.vo.SysUserDetailVO;
import com.fs.luban.module.sys.entity.vo.SysUserVO;
import com.fs.luban.module.sys.mapper.SysUserMapper;
import com.fs.luban.module.sys.service.SysUserService;

/**
 * <p>
 * 系统-用户表 服务实现类
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

	@Autowired
	private SysUserMapper sysUserMapper;
	
	@Override
	public SysUserDetailVO selectUserByUsername(String username) {
		return sysUserMapper.selectSysUserVOByUsername(username);
	}

	@Override
	public Page<SysUserVO> selectSysUserVOPage(Page<SysUserVO> page) {
		return (Page<SysUserVO>) page.setRecords(sysUserMapper.selectUserVOList(page));
	}

	@Override
	public SysUserVO selectSysUserVOById(Integer id) {
		return sysUserMapper.selectSysUserVOById(id);
	}

	@Override
	public Boolean checkUsername(String username) {
		if(StringUtils.isNotBlank(username)){
			SysUser user = new SysUser();
			user.setUsername(username);
			return count(new QueryWrapper<SysUser>(user)) == 0;
		}
		return false;
	}

}
