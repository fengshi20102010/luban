package com.fs.luban.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fs.luban.module.sys.entity.SysOlog;

/**
 * <p>
 * 系统-操作日志表 Mapper 接口
 * </p>
 *
 * @author fengshi
 * @since 2018-05-10
 */
public interface SysOlogMapper extends BaseMapper<SysOlog> {

}
