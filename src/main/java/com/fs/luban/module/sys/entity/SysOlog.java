package com.fs.luban.module.sys.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统-操作日志表
 * </p>
 *
 * @author fengshi
 * @since 2018-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_olog")
public class SysOlog extends Model<SysOlog> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 操作名称
	 */
	private String name;
	/**
	 * 请求URI
	 */
	@TableField("request_uri")
	private String requestUri;
	/**
	 * 请求方式
	 */
	@TableField("request_method")
	private String requestMethod;
	/**
	 * 请求参数
	 */
	@TableField("request_parameters")
	private String requestParameters;
	/**
	 * 执行时间
	 */
	@TableField("execute_milliseconds")
	private Long executeMilliseconds;
	/**
	 * 操作时间
	 */
	@TableField("operate_time")
	private Date operateTime;
	/**
	 * 操作人
	 */
	@TableField("operate_user")
	private String operateUser;
	/**
	 * 操作人id
	 */
	@TableField("operate_user_id")
	private Integer operateUserId;
	/**
	 * 操作结果{0:失败,1:成功}
	 */
	@TableField("operate_result")
	private Integer operateResult;
	/**
	 * 操作结果消息
	 */
	@TableField("operate_message")
	private String operateMessage;
	/**
	 * 执行类名称
	 */
	@TableField("class_name")
	private String className;
	/**
	 * 执行类方法
	 */
	@TableField("class_methed")
	private String classMethed;
	/**
	 * 客户端信息
	 */
	@TableField("client_info")
	private String clientInfo;
	/**
	 * 备注
	 */
	private String remark;

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
