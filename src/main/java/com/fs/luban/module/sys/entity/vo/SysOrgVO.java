package com.fs.luban.module.sys.entity.vo;

import java.io.Serializable;
import java.util.List;

import com.fs.luban.module.sys.entity.SysOrg;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysOrgVO extends SysOrg implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 子节点
	 */
	private List<SysOrg> childrenList;

}
