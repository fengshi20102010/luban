package com.fs.luban.module.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fs.luban.module.sys.entity.SysResource;
import com.fs.luban.module.sys.entity.vo.SysResourceVO;

/**
 * <p>
 * 系统-资源表 Mapper 接口
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
public interface SysResourceMapper extends BaseMapper<SysResource> {

	/**
	 * @title 查询所有的菜单
	 * @param @return
	 * @return List<SysResourceVO>
	 * @author fengshi
	 */
	List<SysResourceVO> queryResList(@Param("pid") Integer pid);
	
	/**
	 * @title 排序
	 * @author fengshi
	 */
	int sort(@Param("id")Integer id, @Param("pid")Integer pid, @Param("sortNo")Integer sortNo);

}
