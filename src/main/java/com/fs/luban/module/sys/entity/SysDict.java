package com.fs.luban.module.sys.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统-数据字典表
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_dict")
public class SysDict extends Model<SysDict> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 父id
	 */
	private Integer pid;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 值
	 */
	private String value;
	/**
	 * 备注
	 */
	private String remark;
	/**
	 * 类型{1:系统,2:业务,3:其他}
	 */
	private Integer type;
	/**
	 * 状态{0:禁用,1:启用}
	 */
	private Integer status;
	/**
	 * 排序
	 */
	@TableField("sort_no")
	private Integer sortNo;
	/**
	 * 创建时间
	 */
	@TableField("create_time")
	private Date createTime;
	/**
	 * 修改时间
	 */
	@TableField("update_time")
	private Date updateTime;
	/**
	 * 乐观锁
	 */
	@Version
	private Integer version;

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
