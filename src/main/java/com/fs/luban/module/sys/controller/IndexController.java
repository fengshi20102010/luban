package com.fs.luban.module.sys.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.fs.luban.common.controller.CommonController;

/**
 * @title 控制器入口
 * @author fengshi
 */
@Controller
public class IndexController extends CommonController {

	// 页面模板
	final String LOGIN = "login";
	final String IE = "module/sys/ie";
	final String INDEX = "module/sys/index";
	final String WELCOME = "module/sys/welcome";

	/**
	 * @title 登陆页面
	 * @author fengshi
	 */
	@GetMapping("login")
	public String login() {
		return LOGIN;
	}

	/**
	 * @title 不支持ie页面提示
	 * @author fengshi
	 */
	@GetMapping("ie")
	public String ie() {
		return IE;
	}

	/**
	 * @title 主页框架
	 * @author fengshi
	 */
	@GetMapping("/")
	public String index() {
		return INDEX;
	}

	/**
	 * @title 主页信息
	 * @author fengshi
	 */
	@GetMapping("welcome")
	public String welcome() {
		return WELCOME;
	}

}
