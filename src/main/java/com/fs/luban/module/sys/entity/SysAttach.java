package com.fs.luban.module.sys.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统_附件表
 * </p>
 *
 * @author fengshi
 * @since 2018-05-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_attach")
public class SysAttach extends Model<SysAttach> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Integer userId;
    /**
     * 源文件名
     */
    @TableField("source_name")
    private String sourceName;
    /**
     * 文件名
     */
    @TableField("file_name")
    private String fileName;
    /**
     * 文件类型
     */
    @TableField("file_type")
    private String fileType;
    /**
     * 磁盘路径
     */
    @TableField("file_path")
    private String filePath;
    /**
     * 文件大小
     */
    @TableField("file_size")
    private Long fileSize;
    /**
     * 附件地址
     */
    @TableField("attach_url")
    private String attachUrl;
    /**
     * 关联模块
     */
    @TableField("relation_key")
    private String relationKey;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
