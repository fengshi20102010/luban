package com.fs.luban.module.sys.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fs.luban.module.sys.entity.SysRole;
import com.fs.luban.module.sys.mapper.SysRoleMapper;
import com.fs.luban.module.sys.service.SysRoleService;

/**
 * <p>
 * 系统-角色表 服务实现类
 * </p>
 *
 * @author fengshi
 * @since 2018-04-12
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

}
