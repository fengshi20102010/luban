package com.fs.luban.common.support;

import lombok.Data;

/**
 * @title @param <T> 获取单个实体的Json Response
 * @author fengshi
 */
@Data
public class JsonEntityResult<T> extends JsonResult {

	private T entity = null;

	public JsonEntityResult() {
		super();
	}

	public JsonEntityResult(T entity) {
		super();
		this.entity = entity;
	}

}
