package com.fs.luban.common.support;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

import com.fs.luban.module.sys.entity.SysOrg;

/**
 * @author fengshi
 * @title @param <T> 获取列表的Json Response
 */
@Data
public class JsonListResult<T> extends JsonResult {

    private Long total = 0l;
    private List<T> rows = new ArrayList<T>();

    public JsonListResult() {
        super();
    }

    public JsonListResult(Long total, List<T> rows) {
        super();
        this.total = total;
        this.rows = rows;
    }


}
