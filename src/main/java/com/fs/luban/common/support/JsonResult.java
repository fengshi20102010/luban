package com.fs.luban.common.support;

import java.util.Map;

import com.google.common.collect.Maps;
import lombok.Data;

/**
 * @title JSON Response基类
 * @author fengshi
 */
@Data
public class JsonResult {

	private boolean success = true;

	private String code = "";

	private String message = "";

	private Map<Object, Object> data = Maps.newHashMap();

	public JsonResult() {
		super();
	}

	public JsonResult(boolean success) {
		super();
		this.success = success;
	}

	public JsonResult(String code, String message) {
		super();
		this.code = code;
		this.message = message;
		this.success = false;
	}

	public JsonResult appendData(Object key, Object value) {
		this.data.put(key, value);
		return this;
	}

	public JsonResult appendData(Map<?, ?> map) {
		this.data.putAll(map);
		return this;
	}

}
