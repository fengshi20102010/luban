package com.fs.luban.common;

import lombok.Getter;

public final class Constant {

	/**
	 * 全局定义
	 */
	public static final class Global {

		/** 状态 */
		@Getter
		public static enum State {
			Enable(1, "启用"), Disable(0, "禁用");

			private int value;
			private String title;

			private State(int value, String title) {
				this.value = value;
				this.title = title;
			}

		}
	}

	public static final class SysUser {

		/** 用户状态 */
		@Getter
		public static enum States {

			Disable(0, "禁用"), Enable(1, "启用"), Locked(2, "锁定");

			private int status;
			private String name;

			private States(int status, String name) {
				this.status = status;
				this.name = name;
			}
		}

	}

	public static final class SysResource {

		// 请求方法
		@Getter
		public static enum Methed {

			ALL(0, ""), GET(1, "GET"), POST(2, "POST"), UPDATE(3, "UPDATE"), DELETE(4, "DELETE"), OTHER(5, "OTHER");

			private int code;
			private String name;

			private Methed(int code, String name) {
				this.code = code;
				this.name = name;
			}

			public static String getNameByCode(int code) {
				for (Methed methed : Methed.values()) {
					if (methed.getCode() == code) {
						return methed.getName();
					}
				}
				return null;
			}

		}
	}

}
