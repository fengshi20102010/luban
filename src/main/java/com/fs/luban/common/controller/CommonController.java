package com.fs.luban.common.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fs.luban.common.support.JsonListResult;
import com.fs.luban.common.support.JsonResult;

public class CommonController {

	protected Logger log = LogManager.getLogger();

	@Autowired
	private HttpServletRequest request;

	/**
	 * 初始化绑定数据，加入字符串转日期
	 * 
	 * @param request
	 * @param binder
	 */
	@InitBinder
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		CustomDateEditor dateEditor = new CustomDateEditor(format, true);
		binder.registerCustomEditor(Date.class, dateEditor);
	}

	/**
	 * 分页
	 * 
	 * @param request
	 * @param pageSize
	 * @return
	 */
	protected <T> Page<T> getPage(int pageSize) {
		int currentPage = 1;
		if (StringUtils.isNotBlank(request.getParameter("pageNumber"))) {
			try {
				currentPage = Integer.parseInt(request.getParameter("pageNumber"));
			} catch (Exception ex) {
				log.info("分页参数转换失败！");
			}
		}
		Page<T> pageInfo = new Page<T>(currentPage, pageSize);
		final String sortName = request.getParameter("sortName");
		if (StringUtils.isNotBlank(sortName)) {
			final String sort = request.getParameter("sortOrder");
			if (StringUtils.isNotBlank(sort)) {
				if ("asc".equalsIgnoreCase(sort)) {
					pageInfo.setAsc(sortName);
				} else {
					pageInfo.setDesc(sortName);
				}
			} else {
				pageInfo.setDesc(sortName);
			}
		}
		return pageInfo;
	}

	/**
	 * 分页
	 * 
	 * @param request
	 * @return
	 */
	protected <T> Page<T> getPage() {
		int pageSize = 10;
		if (StringUtils.isNotBlank(request.getParameter("pageSize"))) {
			try {
				pageSize = Integer.parseInt(request.getParameter("pageSize"));
			} catch (Exception ex) {
				log.info("分页参数转换失败！");
			}
		}
		return getPage(pageSize);
	}

	/**
	 * 将page转换为JsonListResult
	 * 
	 * @param iPage
	 * @return
	 */
	protected <T> JsonListResult<T> toJsonListResult(IPage<T> iPage) {
		JsonListResult<T> result = new JsonListResult<T>();
		if (iPage.getTotal() > 0) {
			result.setCode("0");
			result.setMessage("获取数据成功");
		}
		result.setTotal(iPage.getTotal());
		result.setRows(iPage.getRecords());
		return result;
	}

	/**
	 * 操作成功
	 * 
	 * @return JsonResult
	 */
	protected JsonResult operSuccess() {
		return operSuccess("操作成功", "0");
	}

	/**
	 * 操作成功
	 * 
	 * @return JsonResult
	 */
	protected JsonResult operSuccess(String msg, String code) {
		return operSuccess(msg, code, null);
	}

	/**
	 * 操作成功
	 * 
	 * @return JsonResult
	 */
	protected JsonResult operSuccess(String msg, String code, Map<String, Object> map) {
		JsonResult result = new JsonResult(true);
		result.setMessage(msg);
		result.setCode(code);
		if (!CollectionUtils.isEmpty(map)) {
			result.appendData(map);
		}
		return result;
	}

	/**
	 * 操作失败
	 * 
	 * @return JsonResult
	 */
	protected JsonResult operFailure(String msg, String code) {
		return operFailure(msg, code, null);
	}

	/**
	 * 操作失败
	 * 
	 * @return JsonResult
	 */
	protected JsonResult operFailure(String msg, String code, Map<String, Object> map) {
		JsonResult result = new JsonResult(false);
		result.setMessage(msg);
		result.setCode(code);
		if (!CollectionUtils.isEmpty(map)) {
			result.appendData(map);
		}
		return result;
	}

}
