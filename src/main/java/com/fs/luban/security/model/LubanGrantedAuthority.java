package com.fs.luban.security.model;

import org.springframework.security.core.GrantedAuthority;

import lombok.Data;

/**
 * @title 鲁班自定义GrantedAuthority
 * @Description 由于原生的GrantedAuthority不支持restful，故重写该方法
 * @author fengshi
 */
@Data
public class LubanGrantedAuthority implements GrantedAuthority {

	private static final long serialVersionUID = 1L;
	/**
	 * 权限标识
	 */
	private final String permission;
	/**
	 * 访问地址
	 */
	private final String url;
	/**
	 * 请求方法
	 */
	private String method;

	public LubanGrantedAuthority(String permission, String url, String method) {
		super();
		this.permission = permission;
		this.url = url;
		this.method = method;
	}

	@Override
	public String getAuthority() {
		return this.permission;
	}

}
