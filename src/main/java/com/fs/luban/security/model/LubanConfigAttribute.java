package com.fs.luban.security.model;

import org.springframework.security.access.ConfigAttribute;

import lombok.Data;

/**
 * @title 鲁班自定义ConfigAttribute
 * @Description 鲁班自定义ConfigAttribute
 * @author fengshi
 */
@Data
public class LubanConfigAttribute implements ConfigAttribute {

	private static final long serialVersionUID = 1L;

	/**
	 * 地址
	 */
	private String url;

	/**
	 * 请求类型
	 */
	private String method;

	public LubanConfigAttribute(String url, String method) {
		super();
		this.url = url;
		this.method = method;
	}

	@Override
	public String getAttribute() {
		return this.url;
	}

}
