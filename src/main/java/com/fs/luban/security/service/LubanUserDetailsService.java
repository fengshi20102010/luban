package com.fs.luban.security.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.fs.luban.module.sys.entity.vo.SysResourceVO;
import com.fs.luban.module.sys.entity.vo.SysUserDetailVO;
import com.fs.luban.module.sys.service.SysUserService;
import com.fs.luban.security.model.LubanUserDetails;
import com.google.common.collect.Lists;

/**
 * @title 鲁班用户认证 service
 * @author fengshi
 */
@Service("userDetailsService")
public class LubanUserDetailsService implements UserDetailsService {

	@Autowired
	private SysUserService sysUserService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// 查询用户
		SysUserDetailVO sysUser = sysUserService.selectUserByUsername(username);
		if (null != sysUser) {
			LubanUserDetails userDetails = new LubanUserDetails();
			BeanUtils.copyProperties(sysUser, userDetails);
			// 查询角色对应的可用资源
			userDetails.setResList(getResListById(sysUser.getId()));
			return userDetails;
		}
		throw new UsernameNotFoundException("not find userInfo");
	}

	private List<SysResourceVO> getResListById(Integer userId) {
		List<SysResourceVO> resList = Lists.newArrayList();
		SysResourceVO vo = new SysResourceVO();
		vo.setId(9);
		vo.setMethod(1);
		vo.setPermission("sys:res:list");
		vo.setUrl("/sys/sysResource/list");
		resList.add(vo);
		return resList;
	}

}
