package com.fs.luban.security.handler;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;

import com.fs.luban.common.support.JsonResult;
import com.fs.luban.util.RequestUtils;
import com.fs.luban.util.mapper.JsonMapper;

/**
 * @title 认证失败Handler
 * @Description 认证失败Handler
 * @author fengshi
 */
public class LubanAccessDeniedHandler extends AccessDeniedHandlerImpl {

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {
		if (RequestUtils.isAjax(request)) {
			response.setStatus(HttpStatus.OK.value());
			response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
			PrintWriter printWriter = response.getWriter();
			// 回填信息
			JsonResult result = new JsonResult(String.valueOf(HttpStatus.FORBIDDEN.value()), accessDeniedException.getLocalizedMessage());
			result.setSuccess(false);
			printWriter.write(JsonMapper.nonDefaultMapper().toJson(result));
			printWriter.flush();
			printWriter.close();
			return;
		}
		super.handle(request, response, accessDeniedException);
	}

}
