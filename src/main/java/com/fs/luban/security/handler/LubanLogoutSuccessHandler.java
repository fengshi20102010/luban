package com.fs.luban.security.handler;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import com.fs.luban.common.support.JsonResult;
import com.fs.luban.util.RequestUtils;
import com.fs.luban.util.mapper.JsonMapper;

/**
 * @title 退出登陆成功Handler
 * @Description 退出登陆成功Handler
 * @author fengshi
 */
public class LubanLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		if (RequestUtils.isAjax(request)) {
			response.setStatus(HttpStatus.OK.value());
			response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
			PrintWriter printWriter = response.getWriter();
			JsonResult result = new JsonResult("0", "登出成功");
			printWriter.write(JsonMapper.nonDefaultMapper().toJson(result));
			printWriter.flush();
			printWriter.close();
			return;
		}
		super.onLogoutSuccess(request, response, authentication);
	}

}
