package com.fs.luban.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;

import com.fs.luban.security.model.LubanUserDetails;

/**
 * @title session门面类
 * @author fengshi
 */
public class SessionFace {

	/**
	 * @title 获取 session Id
	 * @author fengshi
	 */
	public static String getSessionId(HttpServletRequest request) {
		return request.getSession().getId();
	}

	/**
	 * @title 获取session属性
	 * @author fengshi
	 */
	public static Object getAttribute(HttpServletRequest request, String key) {
		return request.getSession().getAttribute(key);
	}

	/**
	 * @title 设置session 属性
	 * @author fengshi
	 */
	public static void setAttribute(HttpServletRequest request, String key, String value) {
		request.getSession().setAttribute(key, value);
	}

	/**
	 * @title 设置session 属性
	 * @author fengshi
	 */
	public static void setAttribute(HttpServletRequest request, String key, Object obj) {
		request.getSession().setAttribute(key, obj);
	}

	/**
	 * @title 删除session属性
	 * @author fengshi
	 */
	public static void removeAttribute(HttpServletRequest request, String key) {
		request.getSession().removeAttribute(key);
	}

	/**
	 * @title 获取当前会话用户
	 * @author fengshi
	 */
	public static LubanUserDetails getSessionUser() {
		Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (object instanceof LubanUserDetails) {
			return (LubanUserDetails) object;
		}
		return null;
	}

	/**
	 * @title 获取当前会话用户是否登录
	 * @author fengshi
	 */
	public static Boolean isUserLogin() {
		return getSessionUser() != null;
	}

	/**
	 * @title 获取当前会话的组织id
	 * @author fengshi
	 */
	public static Integer getOrgId() {
		LubanUserDetails user = getSessionUser();
		if (null != user) {
			return user.getOrgId();
		}
		return null;
	}

	/**
	 * @title 获取当前会话的角色id
	 * @author fengshi
	 */
	public static Integer getRoleId() {
		LubanUserDetails user = getSessionUser();
		if (null != user) {
			return user.getRoleId();
		}
		return null;
	}

}
