package com.fs.luban.log.service;

import com.fs.luban.module.sys.entity.SysOlog;

/**
 * @title 操作日志记录service
 * @Description 操作日志记录service
 * @author fengshi
 */
public interface LogWork {

	void doWork(SysOlog sysOlog);

}
