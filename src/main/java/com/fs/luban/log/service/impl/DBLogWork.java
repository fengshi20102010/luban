package com.fs.luban.log.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

import com.fs.luban.log.service.LogWork;
import com.fs.luban.module.sys.entity.SysOlog;
import com.fs.luban.module.sys.service.SysOlogService;

@Service("logWork")
public class DBLogWork extends TimerTask implements LogWork {

	@Autowired
	private SysOlogService sysOlogService;

	private List<SysOlog> list = new ArrayList<SysOlog>();
	private int limit = 0;
	private int seconds = 10;
	private long time = 0;
	private Timer timer = new Timer();

	@PostMapping
	public void init() {
		timer.schedule(this, seconds * 1000, seconds * 1000);
	}

	@PreDestroy
	public void destroy() {
		timer.cancel();
	}

	@Override
	public void doWork(SysOlog sysOlog) {

		synchronized (list) {
			list.add(sysOlog);
			if (list.size() >= limit) {
				batchInsert();
			}
		}

	}

	private void batchInsert() {
		List<SysOlog> paramList = new ArrayList<SysOlog>();
		synchronized (list) {
			paramList.addAll(list);
			list.clear();
		}
		time = System.currentTimeMillis();
		new Thread(new WorkThread(sysOlogService, paramList)).start();
	}

	@Override
	public void run() {
		if (list.size() >= 1 && System.currentTimeMillis() - time >= 1000 * seconds) {
			batchInsert();
		}
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	private class WorkThread implements Runnable {

		private SysOlogService sysOlogService;
		private List<SysOlog> list;

		private WorkThread(SysOlogService sysOlogService, List<SysOlog> list) {
			this.sysOlogService = sysOlogService;
			this.list = list;
		}

		@Override
		public void run() {
			sysOlogService.saveBatch(list);
		}
	}

}
