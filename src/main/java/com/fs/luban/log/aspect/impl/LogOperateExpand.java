package com.fs.luban.log.aspect.impl;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.fs.luban.log.aspect.LogAdviceExpand;
import com.fs.luban.module.sys.entity.SysOlog;
import com.fs.luban.security.SessionFace;
import com.fs.luban.security.model.LubanUserDetails;

@Component
public class LogOperateExpand implements LogAdviceExpand {

	@Override
	public void expand(HttpServletRequest request, SysOlog olog) {
		// 记录操作用户
		LubanUserDetails user = SessionFace.getSessionUser();
		if (user != null) {
			olog.setOperateUserId(user.getId());
			olog.setOperateUser(user.getName());
		}
	}

}
