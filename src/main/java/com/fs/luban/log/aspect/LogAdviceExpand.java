package com.fs.luban.log.aspect;

import javax.servlet.http.HttpServletRequest;

import com.fs.luban.module.sys.entity.SysOlog;

/**
 * @title 日志自定义扩展
 * @author fengshi
 */
public interface LogAdviceExpand {

	void expand(HttpServletRequest request, SysOlog olog);

}
