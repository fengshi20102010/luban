package com.fs.luban.log.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @title 操作日志注解
 * @Description 操作日志注解
 * @author fengshi
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Olog {

	/** 名称 */
	String name();

	/** 备注 */
	String remark() default "";

	/** 是否需要记录参数 */
	boolean needRecordParameter() default true;

	/** 忽略的参数 */
	String[] ignorParam() default "";

	/** 参数中文对应 */
	String[] paramMapping() default "";

}
