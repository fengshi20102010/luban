package com.fs.luban.exception;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * 自定义全局异常处理 BaseGlobalExceptionHandler 已经做ajax调用处理（网页请求返回页面，ajax请求返回json）
 * 该全局异常处理主要针对 rest API 调用异常处理
 */
@ControllerAdvice
public class GlobalExceptionHandler extends BaseGlobalExceptionHandler {

	@Resource
	private HttpServletRequest req;
	@Resource
	private HttpServletResponse rsp;

	/**
	 * 404
	 * 
	 * @param req
	 * @param rsp
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ModelAndView handle404Error(Exception e) throws Exception {
		return handleError(req, rsp, e, "error/404", HttpStatus.NOT_FOUND);
	}

	/**
	 * 400 - Bad Request
	 * 
	 * @param req
	 * @param rsp
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ModelAndView handle400Error(Exception e) throws Exception {
		return handleError(req, rsp, e, "error/500", HttpStatus.NOT_FOUND);
	}

	/**
	 * 405 - Method Not Allowed
	 * 
	 * @param req
	 * @param rsp
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	@ExceptionHandler(HttpRequestMethodNotSupportedException.class)
	public ModelAndView handle405Error(Exception e) throws Exception {
		return handleError(req, rsp, e, "error/500", HttpStatus.NOT_FOUND);
	}

	/**
	 * 415 - Unsupported Media Type
	 * 
	 * @param req
	 * @param rsp
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
	@ExceptionHandler(HttpMediaTypeNotSupportedException.class)
	public ModelAndView handle415Error(Exception e) throws Exception {
		return handleError(req, rsp, e, "error/500", HttpStatus.NOT_FOUND);
	}

	/**
	 * 500
	 * 
	 * @param req
	 * @param rsp
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ModelAndView handle500Error(Exception e) throws Exception {
		return handleError(req, rsp, e, "error/500", HttpStatus.INTERNAL_SERVER_ERROR);
	}

}
