package com.fs.luban;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @title 鲁班主程序启动类
 * ClassName: Application 
 * @author fengshi
 * @date 2018年4月4日
 */
@SpringBootApplication
public class Application  {

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}
